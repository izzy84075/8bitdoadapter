#pragma once

#include <stdint.h>
#include "GenericController.h"

typedef struct {
    uint16_t dataWord;  //All digital buttons
    uint8_t rightTrigger;
    uint8_t leftTrigger;
    uint8_t joystickX;
    uint8_t joystickY;
    uint8_t joystick2X;
    uint8_t joystick2Y;
} SDC_DATA_t;

void GenericControllerToSDC(GenericController* ControllerInput, SDC_DATA_t* SDCOutput);