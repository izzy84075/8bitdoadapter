#pragma once

#include <stdint.h>
#include "GenericController.h"

typedef struct {
    uint8_t dataByte1;  //B through Right
    uint8_t dataByte2;  //A through R, in the top four bits.
} SNES_DATA_t;

void GenericControllerToSNES(GenericController* ControllerInput, SNES_DATA_t* SNESOutput);