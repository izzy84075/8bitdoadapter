#pragma once

#include <stdint.h>
#include "GenericController.h"

typedef struct {
    uint8_t dataByte1;  //R through Y, in the upper four bits
    uint8_t dataByte2;  //Start through B, in the upper four bits
    uint8_t dataByte3;  //Right through Up, in the upper four bits
    uint8_t dataByte4;  //L and three ID bits, in the upper four bits
} SS6_DATA_t;

void GenericControllerToSS6(GenericController* ControllerInput, SS6_DATA_t* SS6Output);