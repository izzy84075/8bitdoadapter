#pragma once

#include <stdint.h>
#include "GenericController.h"

typedef struct {
    uint8_t dataByte;  //All six buttons, in the top six bits
} SMS_DATA_t;

void GenericControllerToSMS(GenericController* ControllerInput, SMS_DATA_t* SMSOutput);