
/**
 ******************************************************************************
 * @file      startup_STM32F301x8_Gnu
 * @author    Izzy
 * @version   V1.0
 * @date      01/17/2019
 * @brief     N572 devices startup code.
 *            This module performs:
 *                - Set the initial SP
 *                - Set the vector table entries with the exceptions ISR address
 *                - Initialize data and bss
 *                - Setup the microcontroller system.
 *                - Call the application's entry point.
 *            After Reset the Cortex-M4 processor is in Thread mode,
 *            priority is Privileged, and the Stack is set to Main.
 *******************************************************************************
 */
#include <stdint.h>

/*----------Macro definition--------------------------------------------------*/
#define WEAK __attribute__ ((weak))

/*----------Declaration of the default fault handlers-------------------------*/
/* System exception vector handler */
__attribute__ ((used))
void WEAK  Reset_Handler(void);
void WEAK  NMI_Handler(void);
void WEAK  HardFault_Handler(void);
void WEAK  MemManage_Handler(void);
void WEAK  BusFault_Handler(void);
void WEAK  UsageFault_Handler(void);

void WEAK  SVC_Handler(void);
void WEAK  DebugMon_Handler(void);

void WEAK  PendSV_Handler(void);
void WEAK  SysTick_Handler(void);

void WEAK  WWDG_IRQHandler(void);
void WEAK  PVD_IRQHandler(void);
void WEAK  TAMPER_IRQHandler(void);
void WEAK  RTC_WKUP_IRQHandler(void);
void WEAK  FLASH_IRQHandler(void);
void WEAK  RCC_IRQHandler(void);
void WEAK  EXTI0_IRQHandler(void);
void WEAK  EXTI1_IRQHandler(void);
void WEAK  EXTI2_IRQHandler(void);
void WEAK  EXTI3_IRQHandler(void);
void WEAK  EXTI4_IRQHandler(void);
void WEAK  DMA1_Channel1_IRQHandler(void);
void WEAK  DMA1_Channel2_IRQHandler(void);
void WEAK  DMA1_Channel3_IRQHandler(void);
void WEAK  DMA1_Channel4_IRQHandler(void);
void WEAK  DMA1_Channel5_IRQHandler(void);
void WEAK  DMA1_Channel6_IRQHandler(void);
void WEAK  DMA1_Channel7_IRQHandler(void);
void WEAK  ADC1_2_IRQHandler(void);
void WEAK  USB_HP_CAN1_TX_IRQHandler(void);
void WEAK  USB_LP_CAN1_RX0_IRQHandler(void);
void WEAK  CAN1_RX1_IRQHandler(void);
void WEAK  CAN1_SCE_IRQHandler(void);
void WEAK  EXTI9_5_IRQHandler(void);
void WEAK  TIM1_BRK_IRQHandler(void);
void WEAK  TIM1_UP_IRQHandler(void);
void WEAK  TIM1_TRG_COM_IRQHandler(void);
void WEAK  TIM1_CC_IRQHandler(void);
void WEAK  TIM2_IRQHandler(void);
void WEAK  TIM3_IRQHandler(void);

void WEAK  I2C1_EV_IRQHandler(void);
void WEAK  I2C1_ER_IRQHandler(void);

void WEAK  SPI1_IRQHandler(void);

void WEAK  USART1_IRQHandler(void);
void WEAK  USART2_IRQHandler(void);

void WEAK  EXTI15_10_IRQHandler(void);
void WEAK  RTC_Alarm_IRQHandler(void);
void WEAK  USBWakeUp_IRQHandler(void);

void WEAK BootRAM(void);

/*----------Symbols defined in linker script----------------------------------*/
extern uint32_t _start_ram;
extern uint32_t _eram;
extern uint32_t __etext;
extern uint32_t __data_start__;
extern uint32_t __data_end__;
extern uint32_t __bss_start__;
extern uint32_t __bss_end__;
extern uint32_t __StackTop;

typedef void( *pFunc )( void );

/*----------Function prototypes-----------------------------------------------*/
extern int main(void);           /*!< The entry point for the application */
void Default_Reset_Handler(void);   /*!< Default reset handler */
static void Default_Handler(void);  /*!< Default exception handler */

/**
  *@brief The minimal vector table for a Cortex M3.  Note that the proper constructs
  *       must be placed on this to ensure that it ends up at physical address
  *       0x00000000.
  */
const pFunc __Vectors[] __attribute__ ((section(".vectors"))) = {
  /*----------Core Exceptions------------------------------------------------ */
  (pFunc)((uint32_t)&__StackTop),      /*!< The initial stack pointer         */
  Reset_Handler,                       /*!< The reset handler                 */
  NMI_Handler,                         /*!< The NMI handler                   */
  HardFault_Handler,                   /*!< The hard fault handler            */
  MemManage_Handler,                   /*!< Memory Management Fault           */
  BusFault_Handler,                    /*!< Bus Arbitration Fault                          */
  UsageFault_Handler,                  /*!< Reserved                          */
  0,                                   /*!< Reserved                          */
  0,                                   /*!< Reserved                          */
  0,                                   /*!< Reserved                          */
  0,                                   /*!< Reserved                          */
  SVC_Handler,                         /*!< SVCall handler                    */
  DebugMon_Handler,                    /*!< Reserved                          */
  0,                                   /*!< Reserved                          */
  PendSV_Handler,                      /*!< The PendSV handler                */
  SysTick_Handler,                     /*!< The SysTick handler               */

  /*----------External Exceptions---------------------------------------------*/
  //0-3
 WWDG_IRQHandler,
 PVD_IRQHandler,
 TAMPER_IRQHandler,
 RTC_WKUP_IRQHandler,
 //4-7
 FLASH_IRQHandler,
 RCC_IRQHandler,
 EXTI0_IRQHandler,
 EXTI1_IRQHandler,
 //8-11
 EXTI2_IRQHandler,
 EXTI3_IRQHandler,
 EXTI4_IRQHandler,
 DMA1_Channel1_IRQHandler,
 //12-15
 DMA1_Channel2_IRQHandler,
 DMA1_Channel3_IRQHandler,
 DMA1_Channel4_IRQHandler,
 DMA1_Channel5_IRQHandler,
 //16-19
 DMA1_Channel6_IRQHandler,
 DMA1_Channel7_IRQHandler,
 ADC1_2_IRQHandler,
 USB_HP_CAN1_TX_IRQHandler,
 //20-23
 USB_LP_CAN1_RX0_IRQHandler,
 CAN1_RX1_IRQHandler,
 CAN1_SCE_IRQHandler,
 EXTI9_5_IRQHandler,
 //24-27
 TIM1_BRK_IRQHandler,
 TIM1_UP_IRQHandler,
 TIM1_TRG_COM_IRQHandler,
 TIM1_CC_IRQHandler,
 //28-31
 TIM2_IRQHandler,
 TIM3_IRQHandler,
 Default_Handler,
 I2C1_EV_IRQHandler,
 //32-35
 I2C1_ER_IRQHandler,
 Default_Handler,
 Default_Handler,
 SPI1_IRQHandler,
 //36-39
 Default_Handler,
 USART1_IRQHandler,
 USART2_IRQHandler,
 Default_Handler,
 EXTI15_10_IRQHandler,
 RTC_Alarm_IRQHandler,
 USBWakeUp_IRQHandler,
 Default_Handler,
 Default_Handler,
 Default_Handler,
 Default_Handler,
 Default_Handler,
 Default_Handler,
 Default_Handler,
 BootRAM,
};


uint32_t randomSeed __attribute__((section(".uninit")));

/**
  * @brief  This is the code that gets called when the processor first
  *         starts execution following a reset event. Only the absolutely
  *         necessary set is performed, after which the application
  *         supplied main() routine is called.
  * @param  None
  * @retval None
  */
void Default_Reset_Handler(void)
{
    uint32_t *pSrc, *pDest;
    uint32_t *pTable __attribute__((unused));

    //Go generate a random seed...
    //XOR in all the stuff before the "uninitialized" section
    pSrc = &_start_ram;
    for( ; pSrc < &__data_end__;pSrc++) {
        randomSeed ^= (*pSrc);
    }
    //And XOR in all the stuff after the "uninitialized" section.
    pSrc = &__bss_start__;
    for( ; pSrc < &_eram;pSrc++) {
        randomSeed ^= (*pSrc);
    }

    /*  Single section scheme.
     *
     *  The ranges of copy from/to are specified by following symbols
     *    __etext: LMA of start of the section to copy from. Usually end of text
     *    __data_start__: VMA of start of the section to copy to
     *    __data_end__: VMA of end of the section to copy to
     *
     *  All addresses must be aligned to 4 bytes boundary.
     */
    pSrc  = &__etext;
    pDest = &__data_start__;

    for ( ; pDest < &__data_end__ ; ) {
    *pDest++ = *pSrc++;
    }
    /*  Single BSS section scheme.
    *
    *  The BSS section is specified by following symbols
    *    __bss_start__: start of the BSS section.
    *    __bss_end__: end of the BSS section.
    *
    *  Both addresses must be aligned to 4 bytes boundary.
    */
    pDest = &__bss_start__;

    for ( ; pDest < &__bss_end__ ; ) {
      *pDest++ = 0ul;
    }

  /* Call the application's entry point.*/
  main();
}


/**
  *@brief Provide weak aliases for each Exception handler to the Default_Handler.
  *       As they are weak aliases, any function with the same name will override
  *       this definition.
  */

#pragma weak Reset_Handler = Default_Reset_Handler
#pragma weak NMI_Handler = Default_Handler
#pragma weak HardFault_Handler = Default_Handler
#pragma weak MemManage_Handler = Default_Handler
#pragma weak BusFault_Handler = Default_Handler
#pragma weak UsageFault_Handler = Default_Handler
#pragma weak SVC_Handler = Default_Handler
#pragma weak DebugMon_Handler = Default_Handler
#pragma weak PendSV_Handler = Default_Handler
#pragma weak SysTick_Handler = Default_Handler
#pragma weak WWDG_IRQHandler = Default_Handler
#pragma weak PVD_IRQHandler = Default_Handler
#pragma weak TAMP_STAMP_IRQHandler = Default_Handler
#pragma weak RTC_WKUP_IRQHandler = Default_Handler
#pragma weak FLASH_IRQHandler = Default_Handler
#pragma weak RCC_IRQHandler = Default_Handler
#pragma weak EXTI0_IRQHandler = Default_Handler
#pragma weak EXTI1_IRQHandler = Default_Handler
#pragma weak EXTI2_TSC_IRQHandler = Default_Handler
#pragma weak EXTI3_IRQHandler = Default_Handler
#pragma weak EXTI4_IRQHandler = Default_Handler
#pragma weak DMA1_Channel1_IRQHandler = Default_Handler
#pragma weak DMA1_Channel2_IRQHandler = Default_Handler
#pragma weak DMA1_Channel3_IRQHandler = Default_Handler
#pragma weak DMA1_Channel4_IRQHandler = Default_Handler
#pragma weak DMA1_Channel5_IRQHandler = Default_Handler
#pragma weak DMA1_Channel6_IRQHandler = Default_Handler
#pragma weak DMA1_Channel7_IRQHandler = Default_Handler
#pragma weak ADC1_IRQHandler = Default_Handler
#pragma weak EXTI9_5_IRQHandler = Default_Handler
#pragma weak TIM1_BRK_TIM15_IRQHandler = Default_Handler
#pragma weak TIM1_UP_TIM16_IRQHandler = Default_Handler
#pragma weak TIM1_TRG_COM_TIM17_IRQHandler = Default_Handler
#pragma weak TIM1_CC_IRQHandler = Default_Handler
#pragma weak TIM2_IRQHandler = Default_Handler
#pragma weak I2C1_EV_IRQHandler = Default_Handler
#pragma weak I2C1_ER_IRQHandler = Default_Handler
#pragma weak I2C2_EV_IRQHandler = Default_Handler
#pragma weak I2C2_ER_IRQHandler = Default_Handler
#pragma weak SPI2_IRQHandler = Default_Handler
#pragma weak USART1_IRQHandler = Default_Handler
#pragma weak USART2_IRQHandler = Default_Handler
#pragma weak USART3_IRQHandler = Default_Handler
#pragma weak RTC_Alarm_IRQHandler = Default_Handler
#pragma weak SPI3_IRQHandler = Default_Handler
#pragma weak TIM6_DAC_IRQHandler = Default_Handler
#pragma weak COMP2_IRQHandler = Default_Handler
#pragma weak COMP4_6_IRQHandler = Default_Handler
#pragma weak I2C3_EV_IRQHandler = Default_Handler
#pragma weak I2C3_ER_IRQHandler = Default_Handler
#pragma weak FPU_IRQHandler = Default_Handler

/**
  * @brief  This is the code that gets called when the processor receives an
  *         unexpected interrupt.  This simply enters an infinite loop,
  *         preserving the system state for examination by a debugger.
  * @param  None
  * @retval None
  */
static void Default_Handler(void)
{
  /* Go into an infinite loop. */
  while (1)
  {
  }
}
