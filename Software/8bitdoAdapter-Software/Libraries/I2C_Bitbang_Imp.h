#ifndef __I2C_BITBANG_IMP_H__
#define __I2C_BITBANG_IMP_H__

#include <stdint.h>
#include <stdbool.h>

#include "../Application/main.h"

#define I2C_DEVICE_SUPPORTS_RESTART false
#define I2C_DEVICE_REQUIRES_DELAY_AFTER_RESTART true

#define I2C_SCL_PORT    I2C_SCK_GPIO_Port
#define I2C_SCL_PIN     I2C_SCK_Pin

#define I2C_SDA_PORT    I2C_SDA_GPIO_Port
#define I2C_SDA_PIN     I2C_SDA_Pin

//Use this function to set the I2C speed.
#pragma GCC push_options
#pragma GCC optimize 0
static inline void I2C_IMP_Delay(void) {
    //Do things here to take up time if you need to lock the maximum clock rate.
    //This library implements Clock Stretching if the device supports it, but not all do.
    //For 400KHz "Fast" I2C, this needs to take up 1.25 microseconds
    //For 100KHz "Standard" I2C, this needs to take up 5 microseconds

    /*{
        uint32_t bigCounter = 0;
        while(bigCounter < 1) {
            bigCounter++;
        }
    }*/
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
    asm("NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;");
}

static inline void I2C_IMP_RestartDelay(void) {
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
    I2C_IMP_Delay();
}
#pragma GCC pop_options

static inline void I2C_IMP_SCL_Init(void) {
    //STM_PIN_SetMode(I2C_SCL_PORT, I2C_SCL_PIN, N572_PIN_MODE_OPEN_DRAIN);
    HAL_GPIO_WritePin(I2C_SCL_PORT, I2C_SCL_PIN, true);
}

static inline void I2C_IMP_SDA_Init(void) {
    //STM_PIN_SetMode(I2C_SDA_PORT, I2C_SDA_PIN, N572_PIN_MODE_OPEN_DRAIN);
    HAL_GPIO_WritePin(I2C_SDA_PORT, I2C_SDA_PIN, true);
}

// Return current level of SCL line, 0 or 1
static inline bool I2C_IMP_R_SCL(void) {
    return (bool)(HAL_GPIO_ReadPin(I2C_SCL_PORT, I2C_SCL_PIN) == GPIO_PIN_SET);
}

// Return current level of SDA line, 0 or 1
static inline bool I2C_IMP_R_SDA(void) {
    return (bool)(HAL_GPIO_ReadPin(I2C_SDA_PORT, I2C_SDA_PIN) == GPIO_PIN_SET);
}

// Set SCL between high-impedance and pull-low
static inline void I2C_IMP_W_SCL(bool pullLow) {
    HAL_GPIO_WritePin(I2C_SCL_PORT, I2C_SCL_PIN, !pullLow);
    //I2C_IMP_Delay();
}

// Set SDA between high-impedance and pull-low
static inline void I2C_IMP_W_SDA(bool pullLow) {
    HAL_GPIO_WritePin(I2C_SDA_PORT, I2C_SDA_PIN, !pullLow);
}

#endif
