#ifndef __I2C_BITBANG_H__
#define __I2C_BITBANG_H__

#include <stdint.h>
#include <stdbool.h>

#include "I2C_Bitbang_Imp.h"

void I2C_Initialize(void);  //Set up the pins

void I2C_ClearBus(void);    //If you have reason to suspect the bus might be in an odd state, call this to try and get devices on the bus to a known state

void I2C_WriteRegisterBuffer(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *dataBuffer, uint8_t dataBytes);
void I2C_ReadRegisterBuffer(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *dataBuffer, uint8_t dataBytes);

void I2C_WriteRegister(uint8_t i2cAddress, uint8_t registerAddress, uint8_t value);    //Write a single byte to a register
uint8_t I2C_ReadRegister(uint8_t i2cAddress, uint8_t registerAddress);  //Read a single byte from a register

//ADVANCED USAGE BEYOND THIS POINT

void I2C_WriteBuffer(uint8_t i2cAddress, uint8_t *dataBuffer, uint8_t dataBytes);   //Send an arbitrary buffer to an device
void I2C_ReadBuffer(uint8_t i2cAddress, uint8_t *dataBuffer, uint8_t dataBytes);    //Read an arbitrary buffer from a device

#endif
