#include "WiiExtensionController.h"

typedef enum {
    WIIEXTENSIONCONTROLLER_STATE_DISCONNECTED = 0,
    WIIEXTENSIONCONTROLLER_STATE_INITIALIZING,
    WIIEXTENSIONCONTROLLER_STATE_IDENTIFYING,
    WIIEXTENSIONCONTROLLER_STATE_CALIBRATING,
    WIIEXTENSIONCONTROLLER_STATE_CONFIGURING,
    WIIEXTENSIONCONTROLLER_STATE_CONNECTED,
    WIIEXTENSIONCONTROLLER_STATE_POLLING,
} WIIEXTENSIONCONTROLLER_STATES_t;

const uint8_t WiiExtensionController_I2C_Address = 0x52;
const uint8_t WiiExtensionController_InputData_Address = 0x00;
const uint8_t WiiExtensionController_InputData_Size = 9;
const uint8_t WiiExtensionController_CalibrationData_Address = 0x20;
const uint8_t WiiExtensionController_CalibrationData_Size = 32;
const uint8_t WiiExtensionController_EncryptionData_Address = 0x40;
const uint8_t WiiExtensionController_EncryptionData_Size = 16;
const uint8_t WiiExtensionController_InitializationRegister1_Address = 0xF0;
const uint8_t WiiExtensionController_InitializationRegister1_Data = 0x55;
const uint8_t WiiExtensionController_IDBlock_Address = 0xFA;
const uint8_t WiiExtensionController_IDBlock_Size = 6;
const uint8_t WiiExtensionController_ControllerMinorID_Address = 0xFA;
const uint8_t WiiExtensionController_InitializationRegister2_Address = 0xFB;
const uint8_t WiiExtensionController_InitializationRegister2_Data = 0x00;
const uint8_t WiiExtensionController_IDConstant_Address = 0xFC;
const uint8_t WiiExtensionController_IDConstant_Size = 2;
const uint8_t WiiExtensionController_IDConstant_Data[2] = {0xA4, 0x20};
const uint8_t WiiExtensionController_ControllerDataFormatID_Address = 0xFE;
const uint8_t WiiExtensionController_ControllerMajorID_Address = 0xFF;

volatile uint16_t WiiExtensionController_PollDelayMS = 0;
bool WiiExtensionController_DetectPinState = false;
uint16_t WiiExtensionController_DetectPinCountMS = 0;

WIIEXTENSIONCONTROLLER_STATES_t WiiExtensionController_State = WIIEXTENSIONCONTROLLER_STATE_DISCONNECTED;
uint8_t WiiExtensionController_subState = 0;

volatile WIIEXTENSIONCONTROLLER_TYPES_t WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_NONE;
uint8_t WiiExtensionController_DataFormat = 0;
uint8_t WiiExtensionController_CalibrationBlock[32];
uint8_t WiiExtensionController_TransferBuffer[9];   //The largest known transfer besides the calibration data block, which we want to save separately so we can refer to it later.


extern void I2C_ClearBus(void);

void WiiExtensionController_Tick1MS(void) {
    if(WiiExtensionController_PollDelayMS) {
        WiiExtensionController_PollDelayMS--;
    }

    if(WiiExtensionController_Imp_R_Detect() == WiiExtensionController_DetectPinState) {
        if(WiiExtensionController_DetectPinCountMS < UINT16_MAX) {
            WiiExtensionController_DetectPinCountMS++;
        }
    } else {
        WiiExtensionController_DetectPinCountMS = 0;
        WiiExtensionController_DetectPinState = !WiiExtensionController_DetectPinState;
    }

    if(WiiExtensionController_State == WIIEXTENSIONCONTROLLER_STATE_DISCONNECTED) {
        if( (WiiExtensionController_DetectPinState == true) && (WiiExtensionController_DetectPinCountMS > 1000)) {
            I2C_ClearBus();
            WiiExtensionController_PollDelayMS = 5;
            WiiExtensionController_subState = 5;
            WiiExtensionController_State = WIIEXTENSIONCONTROLLER_STATE_INITIALIZING;
        }
    } else {
        if( (WiiExtensionController_DetectPinState == false) && (WiiExtensionController_DetectPinCountMS > 1000)) {
            WiiExtensionController_State = WIIEXTENSIONCONTROLLER_STATE_DISCONNECTED;
            WiiExtensionController_subState = 0;
            WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_NONE;
            WiiExtensionController_PollDelayMS = 0;
        }
    }
}

void WiiExtensionController_ServiceLoops(void) {
    static bool alreadyRunning = false;
    if(!alreadyRunning) {
        alreadyRunning = true;
        uint8_t minorID = 0;
        uint8_t majorID = 0;
        if(!WiiExtensionController_PollDelayMS) {
            switch(WiiExtensionController_State) {
                default:
                    break;
                case WIIEXTENSIONCONTROLLER_STATE_INITIALIZING:
                    switch(WiiExtensionController_subState) {
                        default:
                            WiiExtensionController_subState = 0;
                            //Fall through
                        case 0:
                            //Send the first initialization byte
                            WiiExtensionController_TransferBuffer[0] = WiiExtensionController_InitializationRegister1_Data;
                            WiiExtensionController_Imp_I2C_Write(WiiExtensionController_I2C_Address, WiiExtensionController_InitializationRegister1_Address, WiiExtensionController_TransferBuffer, 1);
                            WiiExtensionController_subState++;
                            WiiExtensionController_PollDelayMS = 2;
                            break;
                        case 1:
                            //Send the second initialization byte
                            WiiExtensionController_TransferBuffer[0] = WiiExtensionController_InitializationRegister2_Data;
                            WiiExtensionController_Imp_I2C_Write(WiiExtensionController_I2C_Address, WiiExtensionController_InitializationRegister2_Address, WiiExtensionController_TransferBuffer, 1);
                            WiiExtensionController_subState++;
                            WiiExtensionController_PollDelayMS = 2;
                            break;
                        case 2:
                            //Read back the "0xA420" constant to double-check that it's actually a Wii Extension Controller.
                            WiiExtensionController_Imp_I2C_Read(WiiExtensionController_I2C_Address, WiiExtensionController_IDBlock_Address, WiiExtensionController_TransferBuffer, WiiExtensionController_IDBlock_Size);
                            if((WiiExtensionController_TransferBuffer[2] == WiiExtensionController_IDConstant_Data[0]) && (WiiExtensionController_TransferBuffer[3] == WiiExtensionController_IDConstant_Data[1])) {
                                WiiExtensionController_State = WIIEXTENSIONCONTROLLER_STATE_IDENTIFYING;
                                WiiExtensionController_subState = 0;
                            } else {
                                WiiExtensionController_subState = 0;
                                WiiExtensionController_PollDelayMS = 2;
                            }
                            break;
                    }
                    break;
                case WIIEXTENSIONCONTROLLER_STATE_IDENTIFYING:
                    //Read the minor part of the ID
                    //WiiExtensionController_Imp_I2C_Read(WiiExtensionController_I2C_Address, WiiExtensionController_ControllerMinorID_Address, WiiExtensionController_TransferBuffer, 1);
                    minorID = WiiExtensionController_TransferBuffer[0];
                    //Read the major part of the ID
                    //WiiExtensionController_Imp_I2C_Read(WiiExtensionController_I2C_Address, WiiExtensionController_ControllerMajorID_Address, WiiExtensionController_TransferBuffer, 1);
                    majorID = WiiExtensionController_TransferBuffer[5];

                    WiiExtensionController_DataFormat = WiiExtensionController_TransferBuffer[4];

                    if(majorID == 0x00 && minorID == 0x00) {
                        //Nunchuck
                        WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_NUNCHUCK;
                    } else if(majorID == 0x01) {
                        //Classic Controller
                        if(minorID == 0x00) {
                            //Regular Classic Controller
                            WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_CLASSIC_CONTROLLER;
                        } else if(minorID == 0x01) {
                            //Classic Controller Pro
                            WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_CLASSIC_CONTROLLER_PRO;
                        }
                    } else if(majorID == 0x02 && minorID == 0x00) {
                        //Balance Board
                        WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_BALANCE_BOARD;
                    } else if(majorID == 0x03) {
                        if(minorID == 0x00) {
                            //Guitar Hero Guitar
                            WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_GH_GUITAR;
                        } else if(minorID == 0x01) {
                            //Guitar Hero Drums
                            WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_GH_DRUM;
                        } else if(minorID == 0x03) {
                            //DJ Hero Turntable
                            WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_DJ_TURNTABLE;
                        }
                    } else if(majorID == 0x11 && minorID == 0x00) {
                        //TaTaCon Drum
                        WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_TATACON_DRUM;
                    } else if(majorID == 0x13 && minorID == 0xFF) {
                        //Drawsome Graphics Tablet
                        WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_TABLET;
                    } else {
                        WiiExtensionController_Type = WIIEXTENSIONCONTROLLER_TYPE_UNKNOWN;
                    }

                    if(WiiExtensionController_Type != WIIEXTENSIONCONTROLLER_TYPE_UNKNOWN) {
                        WiiExtensionController_State = WIIEXTENSIONCONTROLLER_STATE_CALIBRATING;
                        WiiExtensionController_PollDelayMS = 2;
                    }
                    break;
                case WIIEXTENSIONCONTROLLER_STATE_CALIBRATING:
                    WiiExtensionController_Imp_I2C_Read(WiiExtensionController_I2C_Address, WiiExtensionController_CalibrationData_Address, WiiExtensionController_CalibrationBlock, WiiExtensionController_CalibrationData_Size);
                    WiiExtensionController_State = WIIEXTENSIONCONTROLLER_STATE_CONFIGURING;
                    WiiExtensionController_subState = 1;
                    WiiExtensionController_PollDelayMS = 2;
                    break;
                case WIIEXTENSIONCONTROLLER_STATE_CONFIGURING:
                    switch(WiiExtensionController_subState) {
                        default:
                            WiiExtensionController_subState = 0;
                            //Fall through
                        case 0:
                            //Read the controller's default data format.
                            WiiExtensionController_Imp_I2C_Read(WiiExtensionController_I2C_Address, WiiExtensionController_ControllerDataFormatID_Address, WiiExtensionController_TransferBuffer, 1);
                            WiiExtensionController_DataFormat = WiiExtensionController_TransferBuffer[0];
                            WiiExtensionController_subState++;
                            WiiExtensionController_PollDelayMS = 2;
                            break;
                        case 1:
                            //Do extra initialization, if needed
                            if(WiiExtensionController_Imp_Configure(WiiExtensionController_Type, WiiExtensionController_DataFormat)) {
                                //If the implementation changed the config, go read the data format again.
                                WiiExtensionController_subState = 0;
                            } else {
                                //Otherwise, we're done initializing
                                WiiExtensionController_State = WIIEXTENSIONCONTROLLER_STATE_CONNECTED;
                                WiiExtensionController_subState = 0;
                            }
                            WiiExtensionController_PollDelayMS = 2;
                            break;
                    }
                    break;
                case WIIEXTENSIONCONTROLLER_STATE_CONNECTED:
                    switch(WiiExtensionController_subState) {
                    default:
                        WiiExtensionController_subState = 0;
                        //Fall through
                    case 0:
                        //Start the next controller read cycle
                        WiiExtensionController_TransferBuffer[0] = WiiExtensionController_InputData_Address;
                        WiiExtensionController_Imp_I2C_WriteDirect(WiiExtensionController_I2C_Address, WiiExtensionController_TransferBuffer, 1);
                        WiiExtensionController_subState++;
                        WiiExtensionController_PollDelayMS = 3;
                        break;
                    case 1:
                        //Read the input data
                        WiiExtensionController_Imp_I2C_ReadDirect(WiiExtensionController_I2C_Address, WiiExtensionController_TransferBuffer, WiiExtensionController_InputData_Size);
                        WiiExtensionController_subState = 0;
                        //Go process the new data
                        WiiExtensionController_Imp_DataPolled(WiiExtensionController_Type, WiiExtensionController_DataFormat, WiiExtensionController_TransferBuffer, WiiExtensionController_CalibrationBlock);
                        //Delay a bit before starting the next transfer
                        WiiExtensionController_PollDelayMS = 1;
                    }
                    break;
            }
        }
        alreadyRunning = false;
    }
}

bool WiiExtensionController_CurrentlyConnected(void) {
    if(WiiExtensionController_State != WIIEXTENSIONCONTROLLER_STATE_DISCONNECTED) {
        return true;
    }
    return false;
}

WIIEXTENSIONCONTROLLER_TYPES_t WiiExtensionController_ConnectedType(void) {
    return WiiExtensionController_Type;
}
