#pragma once

#include <stdint.h>
#include <stdbool.h>

#include "WiiExtensionController_Imp.h"

void WiiExtensionController_Tick1MS(void);
void WiiExtensionController_ServiceLoops(void);

bool WiiExtensionController_CurrentlyConnected(void);
WIIEXTENSIONCONTROLLER_TYPES_t WiiExtensionController_ConnectedType(void);