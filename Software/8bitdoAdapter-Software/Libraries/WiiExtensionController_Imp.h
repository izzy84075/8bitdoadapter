#pragma once

#include <stdint.h>
#include <stdbool.h>

#include "../Application/Main.h"
#include "../Libraries/I2C_Bitbang.h"

#include "../Controllers/GenericController.h"
#include "../Controllers/WiiClassicController.h"
#include "../Controllers/WiiNunchuck.h"

extern void ServiceLoops(void);

GENERIC_CONTROLLER_t WiiController_Generic;

typedef enum {
    WIIEXTENSIONCONTROLLER_TYPE_NONE = 0,
    WIIEXTENSIONCONTROLLER_TYPE_NUNCHUCK,
    WIIEXTENSIONCONTROLLER_TYPE_CLASSIC_CONTROLLER,
    WIIEXTENSIONCONTROLLER_TYPE_CLASSIC_CONTROLLER_PRO,
    WIIEXTENSIONCONTROLLER_TYPE_BALANCE_BOARD,
    WIIEXTENSIONCONTROLLER_TYPE_GH_GUITAR,
    WIIEXTENSIONCONTROLLER_TYPE_GH_DRUM,
    WIIEXTENSIONCONTROLLER_TYPE_DJ_TURNTABLE,
    WIIEXTENSIONCONTROLLER_TYPE_TATACON_DRUM,
    WIIEXTENSIONCONTROLLER_TYPE_TABLET,

    WIIEXTENSIONCONTROLLER_TYPE_UNKNOWN = 0xFF,
} WIIEXTENSIONCONTROLLER_TYPES_t;

extern const uint8_t WiiExtensionController_I2C_Address;
extern const uint8_t WiiExtensionController_InputData_Address;
extern const uint8_t WiiExtensionController_InputData_Size;
extern const uint8_t WiiExtensionController_CalibrationData_Address;
extern const uint8_t WiiExtensionController_CalibrationData_Size;
extern const uint8_t WiiExtensionController_EncryptionData_Address;
extern const uint8_t WiiExtensionController_EncryptionData_Size;
extern const uint8_t WiiExtensionController_InitializationRegister1_Address;
extern const uint8_t WiiExtensionController_InitializationRegister1_Data;
extern const uint8_t WiiExtensionController_IDBlock_Address;
extern const uint8_t WiiExtensionController_IDBlock_Size;
extern const uint8_t WiiExtensionController_ControllerMinorID_Address;
extern const uint8_t WiiExtensionController_InitializationRegister2_Address;
extern const uint8_t WiiExtensionController_InitializationRegister2_Data;
extern const uint8_t WiiExtensionController_IDConstant_Address;
extern const uint8_t WiiExtensionController_IDConstant_Size;
extern const uint8_t WiiExtensionController_IDConstant_Data[2];
extern const uint8_t WiiExtensionController_ControllerDataFormatID_Address;
extern const uint8_t WiiExtensionController_ControllerMajorID_Address;
extern uint8_t WiiExtensionController_TransferBuffer[9];

inline bool WiiExtensionController_Imp_R_Detect(void) {
    //Return the state of the DETECT pin. 
    return HAL_GPIO_ReadPin(DET_GPIO_Port, DET_Pin);
}

inline void WiiExtensionController_Imp_I2C_Write(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *dataBuffer, uint8_t dataBytes) {
    I2C_WriteRegisterBuffer(i2cAddress, registerAddress, dataBuffer, dataBytes);
    /*while(I2C_Busy()) {
        ServiceLoops();
    }*/
}

inline void WiiExtensionController_Imp_I2C_Read(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *dataBuffer, uint8_t dataBytes) {
    I2C_ReadRegisterBuffer(i2cAddress, registerAddress, dataBuffer, dataBytes);
    /*while(I2C_Busy()) {
        ServiceLoops();
    }*/
}

inline void WiiExtensionController_Imp_I2C_WriteDirect(uint8_t i2cAddress, uint8_t *dataBuffer, uint8_t dataBytes) {
    I2C_WriteBuffer(i2cAddress, dataBuffer, dataBytes);
}

inline void WiiExtensionController_Imp_I2C_ReadDirect(uint8_t i2cAddress, uint8_t *dataBuffer, uint8_t dataBytes) {
    I2C_ReadBuffer(i2cAddress, dataBuffer, dataBytes);
}

inline void WiiExtensionController_Imp_SetControllerDataFormat(uint8_t dataFormat) {
    WiiExtensionController_TransferBuffer[0] = dataFormat;
    WiiExtensionController_Imp_I2C_Write(WiiExtensionController_I2C_Address, WiiExtensionController_ControllerDataFormatID_Address, WiiExtensionController_TransferBuffer, 1);
}

//Do any work here to configure any odd controller modes.
inline bool WiiExtensionController_Imp_Configure(WIIEXTENSIONCONTROLLER_TYPES_t controllerType, uint8_t dataFormat) {
    switch(controllerType) {
        case WIIEXTENSIONCONTROLLER_TYPE_NUNCHUCK:
            if(dataFormat != 0) {
                WiiExtensionController_Imp_SetControllerDataFormat(0);
                return true;
            }
            break;
        case WIIEXTENSIONCONTROLLER_TYPE_CLASSIC_CONTROLLER:
        case WIIEXTENSIONCONTROLLER_TYPE_CLASSIC_CONTROLLER_PRO:
            //Defaults to Data Format 1, but Data Format 3 gets more analog resolution.
            //Data Format 2 may be supported on some controllers, but 8bitdo controllers act like they've switched modes, but do not.
            //Try to switch to data format 3
            if(dataFormat != 3) {
                WiiExtensionController_Imp_SetControllerDataFormat(3);
                return true;
            }
            break;
    }
    return false;
}

//We got new controller data!
//This function should return the number of milliseconds to delay before the next poll.
//~3ms is the shortest supported by the Wii Motion Plus, unknown what the longest supported is.
inline void WiiExtensionController_Imp_DataPolled(WIIEXTENSIONCONTROLLER_TYPES_t controllerType, uint8_t dataFormat, uint8_t* controllerInputData, uint8_t* controllerCalibrationData) {
    switch(controllerType) {
        case WIIEXTENSIONCONTROLLER_TYPE_NUNCHUCK:
            WiiNunchuckToGeneric(dataFormat, controllerInputData, controllerCalibrationData, &WiiController_Generic);
            break;
        case WIIEXTENSIONCONTROLLER_TYPE_CLASSIC_CONTROLLER:
        case WIIEXTENSIONCONTROLLER_TYPE_CLASSIC_CONTROLLER_PRO:
            WiiClassicControllerToGeneric(dataFormat, controllerInputData, controllerCalibrationData, &WiiController_Generic);
            break;
    }
}
