#include "I2C_Bitbang.h"

//Private function prototypes.
void i2c_start_cond(void);
void i2c_restart_cond(void);
void i2c_stop_cond(void);
bool i2c_write_byte(uint8_t byte);
uint8_t i2c_read_byte(bool sendNACK);

//Private Functions

//Start condition is that data goes low while clock is high
void i2c_start_cond(void) {
    //Pull data low
    I2C_IMP_W_SDA(true);
    I2C_IMP_Delay();
    //Pull clock low
    I2C_IMP_W_SCL(true);
    while(I2C_IMP_R_SCL()) {

    }
    I2C_IMP_Delay();
}

//Restart condition takes data high, then clock high, then data low, then clock low
void i2c_restart_cond(void) {
    //Release data to let it float high
    I2C_IMP_W_SDA(false);
    I2C_IMP_Delay();

    //Release clock to let it float high
    I2C_IMP_W_SCL(false);
    //Wait for the clock to go high. Clock stretching for devices that need it.
    while(!I2C_IMP_R_SCL()) {

    }
    I2C_IMP_Delay();

    //Pull data low
    I2C_IMP_W_SDA(true);
    I2C_IMP_Delay();

    //Pull clock low
    I2C_IMP_W_SCL(true);
    while(I2C_IMP_R_SCL()) {

    }
    I2C_IMP_Delay();
}

//Stop condition is that data goes high while clock is high
void i2c_stop_cond(void) {
    //Pull data low
    I2C_IMP_W_SDA(true);
    I2C_IMP_Delay();
    
    //Release clock to let it float high
    I2C_IMP_W_SCL(false);
    //Wait for the clock to go high. Clock stretching for devices that need it.
	while(!I2C_IMP_R_SCL()) {

	}
	I2C_IMP_Delay();

    //Release data to let it float high
    I2C_IMP_W_SDA(false);
    I2C_IMP_Delay();
}

// Write a byte to I2C bus. Return 0 if ack by the slave.
bool i2c_write_byte(uint8_t byte) {
    uint8_t  bit;
    bool     nack;

    for (bit = 0; bit < 8; bit++) {
        //Send the MSB
        if(byte & 0x80) {
            //Release data to let it float high
            I2C_IMP_W_SDA(false);
            while(!I2C_IMP_R_SDA()) {

            }
        } else {
            //Pull data low
            I2C_IMP_W_SDA(true);
            while(I2C_IMP_R_SDA()) {

            }
        }
        //Shift the data left
        byte <<= 1;

        //Send a clock cycle

        //Release clock to let it float high
        I2C_IMP_W_SCL(false);
        //Wait for the clock to go high. Clock stretching for devices that need it.
		while(!I2C_IMP_R_SCL()) {

		}
		I2C_IMP_Delay();
        //Pull clock low
        I2C_IMP_W_SCL(true);
        while(I2C_IMP_R_SCL()) {

        }
        I2C_IMP_Delay();
    }

    //Get the NACK bit

    //Release data to let it float high
    I2C_IMP_W_SDA(false);
    //Release clock to let it float high
    I2C_IMP_W_SCL(false);
    //Wait for the clock to go high. Clock stretching for devices that need it.
    while(!I2C_IMP_R_SCL()) {

    }
    I2C_IMP_Delay();
    //Read the ACK bit
    nack = I2C_IMP_R_SDA();

    //Pull the clock low
    I2C_IMP_W_SCL(true);
    while(I2C_IMP_R_SCL()) {

    }
    I2C_IMP_Delay();

    return nack;
}

// Read a byte from I2C bus
uint8_t i2c_read_byte(bool sendNACK) {
    uint8_t byte = 0;
    volatile uint8_t bit;

    //For 8 bits...
    for (bit = 0; bit < 8; bit++) {
        //Left-shift the data by 1
        byte <<= 1;

        //Release clock to let it float high
        I2C_IMP_W_SCL(false);
        //Wait for the clock to go high
        while(!I2C_IMP_R_SCL()) {

        }
        I2C_IMP_Delay();

        //Read the bit
        byte |= I2C_IMP_R_SDA();

        //Pull clock low
        I2C_IMP_W_SCL(true);
        while(I2C_IMP_R_SCL()) {

        }
        I2C_IMP_Delay();
    }

    //Handle the ACK/NACK bit
    if(sendNACK) {
    	//Let the data line float high
		I2C_IMP_W_SDA(false);
		//Wait for data line to go high
		while(!I2C_IMP_R_SDA()) {

		}
    } else {
        //Pull the data line low
		I2C_IMP_W_SDA(true);
		//Wait for data line to go low
		while(I2C_IMP_R_SDA()) {

		}
    }

    //Release the clock to let it float high
    I2C_IMP_W_SCL(false);
    //Wait for the clock to go high
    while(!I2C_IMP_R_SCL()) {

    }
    I2C_IMP_Delay();

    //Pull clock low
    I2C_IMP_W_SCL(true);
    //Wait for the clock to go low
    while(I2C_IMP_R_SCL()) {

    }
    //Let the data line float high
    I2C_IMP_W_SDA(false);
    I2C_IMP_Delay();

    return byte;
}

//Public functions

void I2C_WriteBuffer(uint8_t i2cAddress, uint8_t *dataBuffer, uint8_t dataBytes) {
    uint8_t temp;

    //Start condition
    i2c_start_cond();
    //Send the device address and a write bit
    i2c_write_byte((i2cAddress << 1));
    //Write the buffer out
    for(temp = 0;temp < dataBytes;temp++) {
        i2c_write_byte(dataBuffer[temp]);
    }
    //Stop condition
    i2c_stop_cond();
}

void I2C_ReadBuffer(uint8_t i2cAddress, uint8_t *dataBuffer, uint8_t dataBytes) {
    uint8_t temp;

    i2c_start_cond();
    //Send the device address and a read bit
    i2c_write_byte(((i2cAddress << 1) | 0x01));
    //Read all but the last byte of the data with an ACK
    temp = 0;
    while(dataBytes > 1) {
        dataBuffer[temp] = i2c_read_byte(false);
        temp++;
        dataBytes--;
    }
    //And read the last byte with a NACK
    dataBuffer[temp] = i2c_read_byte(true);
    //Stop condition
    i2c_stop_cond();
}

void I2C_WriteRegisterBuffer(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *dataBuffer, uint8_t dataBytes) {
    uint8_t temp;

    i2c_start_cond();

    //Mark this as a write
    i2c_write_byte((i2cAddress << 1));

    i2c_write_byte(registerAddress);

    for(temp = 0;temp < dataBytes;temp++) {
        i2c_write_byte(dataBuffer[temp]);
    }
    
    i2c_stop_cond();
}

void I2C_ReadRegisterBuffer(uint8_t i2cAddress, uint8_t registerAddress, uint8_t *dataBuffer, uint8_t dataBytes) {
    uint8_t temp;

    //Start condition
    i2c_start_cond();
    //Send the device address and a write bit
    i2c_write_byte((i2cAddress << 1));
    //Send the address of the register we want to read
    i2c_write_byte(registerAddress);

    //Repeated Start condition
#if I2C_DEVICE_SUPPORTS_RESTART == true
    i2c_restart_cond();
#else
    i2c_stop_cond();
#endif
#if I2C_DEVICE_REQUIRES_DELAY_AFTER_RESTART == true
    I2C_IMP_RestartDelay();
#endif
#if I2C_DEVICE_SUPPORTS_RESTART == false
    i2c_start_cond();
#endif
    //Send the device address and a read bit
    i2c_write_byte(((i2cAddress << 1) | 0x01));
    //Read all but the last byte of the data with an ACK
    temp = 0;
    while(dataBytes > 1) {
    	dataBuffer[temp] = i2c_read_byte(false);
    	temp++;
    	dataBytes--;
    }
    //And read the last byte with a NACK
    dataBuffer[temp] = i2c_read_byte(true);
    //Stop condition
    i2c_stop_cond();
}

void I2C_WriteRegister(uint8_t i2cAddress, uint8_t registerAddress, uint8_t value) {
    uint8_t temp;

    i2c_start_cond();

    //Mark this as a write
    i2c_write_byte((i2cAddress << 1));

    i2c_write_byte(registerAddress);

    i2c_write_byte(value);
    
    i2c_stop_cond();
}



uint8_t I2C_ReadRegister(uint8_t i2cAddress, uint8_t registerAddress) {
    uint8_t temp;

    //Start condition
    i2c_start_cond();
    //Send the device address and a write bit
    i2c_write_byte((i2cAddress << 1));
    //Send the address we want to read
    i2c_write_byte(registerAddress);
    //Repeated Start condition
#if I2C_DEVICE_SUPPORTS_RESTART == true
    i2c_restart_cond();
#else
    i2c_stop_cond();
#endif
#if I2C_DEVICE_REQUIRES_DELAY_AFTER_RESTART == true
    I2C_IMP_RestartDelay();
#endif
#if I2C_DEVICE_SUPPORTS_RESTART == false
    i2c_start_cond();
#endif
    //Send the device address and a read bit
    i2c_write_byte(((i2cAddress << 1) | 0x01));
    //And read the byte with a NACK
    temp = i2c_read_byte(true);
    //Stop condition
    i2c_stop_cond();
    
    return temp;
}

void I2C_ClearBus(void) {
    uint8_t temp;

    for(temp = 0;temp < 10;temp++) {
        //Pull clock low
        I2C_IMP_W_SCL(true);
        //Wait for clock to go low
        while(I2C_IMP_R_SCL()) {

        }
        I2C_IMP_Delay();

        //Release the clock to let it float high
        I2C_IMP_W_SCL(false);
        //Wait for the clock to go high
        while(!I2C_IMP_R_SCL()) {

        }
        I2C_IMP_Delay();
    }

}

void I2C_Initialize(void) {
    I2C_IMP_SCL_Init();
    I2C_IMP_SDA_Init();

    I2C_ClearBus();
}
