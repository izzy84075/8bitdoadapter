#pragma once

#include <stdint.h>

typedef union {
    uint8_t raw;
    struct {
        uint8_t bit0:1;
        uint8_t bit1:1;
        uint8_t bit2:1;
        uint8_t bit3:1;
        uint8_t bit4:1;
        uint8_t bit5:1;
        uint8_t bit6:1;
        uint8_t bit7:1;
    };
} BITFIELD_t;

typedef union {
    uint8_t raw;
    struct {
        uint8_t up:1;
        uint8_t down:1;
        uint8_t left:1;
        uint8_t right:1;
    };
} DIGITAL_DIAMOND_CLUSTER_t;

typedef union {
    uint8_t raw;
    struct {
        uint8_t topLeft:1;
        uint8_t topRight:1;
        uint8_t bottomLeft:1;
        uint8_t bottomRight:1;
    };
} DIGITAL_SQUARE_CLUSTER_t;

typedef union {
    uint8_t raw;
    struct {
        uint8_t topLeft:1;
        uint8_t topRight:1;
        uint8_t bottomLeft:1;
        uint8_t bottomRight:1;
        uint8_t topCenter:1;
        uint8_t bottomCenter:1;
    };
} DIGITAL_RECTANGLE_CLUSTER_t;

typedef union {
    uint8_t raw;
    struct {
        uint8_t right:1;
        uint8_t left:1;
    } twoButtons;
    struct {
        uint8_t right:1;
        uint8_t bottom:1;
        uint8_t left:1;
        uint8_t top:1;
    } diamond;
    struct {
        uint8_t bottomRight:1;
        uint8_t bottomCenter:1;
        uint8_t bottomLeft:1;
        uint8_t topRight:1;
        uint8_t topCenter:1;
        uint8_t topLeft:1;
    } rectangle;
    struct {
        uint8_t center:1;
        uint8_t bottomLeft:1;
        uint8_t right:1;
        uint8_t top:1;
    } gamecube;
} DIGITAL_FACE_BUTTON_CLUSTERS_t;

typedef struct {
    int8_t X;       //-127 is all the way left, 0 is centered, 127 is all the way right
    int8_t Y;       //-127 is all the way down, 0 is centered, 127 is all the way up
} ANALOG_XY_t;

typedef struct {
    float X;      // Negative values are left, 0 is centered, positive values are right
    float Y;      // Negative values are down, 0 is centered, positive values are up
    float Z;      // Negative values are down, 0 is centered, positive values are up
} ANALOG_XYZ_t;

typedef struct {
    DIGITAL_DIAMOND_CLUSTER_t   DPad;               //0 is not-pressed, 1 is pressed.
    DIGITAL_FACE_BUTTON_CLUSTERS_t FaceButtons;        //0 is not-pressed, 1 is pressed.
    DIGITAL_SQUARE_CLUSTER_t    ShoulderButtons;    //0 is not-pressed, 1 is pressed.
    BITFIELD_t                  MiscButtons;        //0 is not-pressed, 1 is pressed.
    ANALOG_XY_t                 LeftJoystick;
    ANALOG_XY_t                 RightJoystick;
    ANALOG_XY_t                 MiscJoystick;
    ANALOG_XYZ_t                Accelerometer;
    ANALOG_XYZ_t                Gyro;
    uint16_t                    MiscAnalog[24];     //0 is all the way not pressed left or down, 32767 is halfway pressed or centered, 65535 is all the way pressed or right or up
} GENERIC_CONTROLLER_t;
