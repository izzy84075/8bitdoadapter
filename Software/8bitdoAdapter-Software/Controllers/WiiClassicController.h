#pragma once

#include <stdint.h>
#include "GenericController.h"

void WiiClassicControllerToGeneric(uint8_t dataFormat, uint8_t* WiiExtensionControllerData, uint8_t* WiiExtensionControllerCalibrationData, GENERIC_CONTROLLER_t* OutputGeneric);
