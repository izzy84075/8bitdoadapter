#include "WiiNunchuck.h"

#define WiiNunchuckControllerData_JoystickXOffset       0
#define WiiNunchuckControllerData_JoystickYOffset       1
#define WiiNunchuckControllerData_AccelerometerXOffset  2
#define WiiNunchuckControllerData_AccelerometerYOffset  3
#define WiiNunchuckControllerData_AccelerometerZOffset  4
#define WiiNunchuckControllerData_ButtonOffset          5

#define WiiNunchuckButton_C                             0x02
#define WiiNunchuckButton_Z                             0x01

#define WiiNunchuckCalibration_AccelerometerX0GOffset   0
#define WiiNunchuckCalibration_AccelerometerY0GOffset   1
#define WiiNunchuckCalibration_AccelerometerZ0GOffset   2
#define WiiNunchuckCalibration_AccelerometerX1GOffset   3
#define WiiNunchuckCalibration_AccelerometerY1GOffset   4
#define WiiNunchuckCalibration_AccelerometerZ1GOffset   5
#define WiiNunchuckCalibration_JoystickXMaxOffset       6
#define WiiNunchuckCalibration_JoystickXMinOffset       7
#define WiiNunchuckCalibration_JoystickXCenterOffset    8
#define WiiNunchuckCalibration_JoystickYMaxOffset       9
#define WiiNunchuckCalibration_JoystickYMinOffset       10
#define WiiNunchuckCalibration_JoystickYCenterOffset    11

void WiiNunchuckToGeneric(uint8_t dataFormat, uint8_t* WiiExtensionControllerData, uint8_t* WiiExtensionControllerCalibrationData, GENERIC_CONTROLLER_t* OutputGeneric) {
    float temp;

    switch(dataFormat) {
        case 0:
            /*if(WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_JoystickXMaxOffset] != 0x00) {
                OutputGeneric->LeftJoystick.X = (int8_t)(((((float)(WiiExtensionControllerData[WiiNunchuckControllerData_JoystickXOffset]) - WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_JoystickXCenterOffset]) /  (WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_JoystickXMaxOffset] - WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_JoystickXMinOffset]))*127));
            } else {*/
                OutputGeneric->LeftJoystick.X = (int8_t)(WiiExtensionControllerData[WiiNunchuckControllerData_JoystickXOffset]) - 128;
            //}
            /*if(WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_JoystickYMaxOffset] != 0x00) {
                OutputGeneric->LeftJoystick.Y = (int8_t)(((((float)(WiiExtensionControllerData[WiiNunchuckControllerData_JoystickYOffset]) - WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_JoystickYCenterOffset]) /  (WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_JoystickYMaxOffset] - WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_JoystickYMinOffset]))*127));
            } else {*/
                OutputGeneric->LeftJoystick.Y = (int8_t)(WiiExtensionControllerData[WiiNunchuckControllerData_JoystickYOffset]) - 128;
            //}

            OutputGeneric->Accelerometer.X = ((((float)WiiExtensionControllerData[WiiNunchuckControllerData_AccelerometerXOffset] - WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_AccelerometerX0GOffset]) / (WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_AccelerometerX1GOffset] - WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_AccelerometerX0GOffset])));
            OutputGeneric->Accelerometer.Y = ((((float)WiiExtensionControllerData[WiiNunchuckControllerData_AccelerometerYOffset] - WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_AccelerometerY0GOffset]) / (WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_AccelerometerY1GOffset] - WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_AccelerometerY0GOffset])));
            OutputGeneric->Accelerometer.Z = ((((float)WiiExtensionControllerData[WiiNunchuckControllerData_AccelerometerZOffset] - WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_AccelerometerZ0GOffset]) / (WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_AccelerometerZ1GOffset] - WiiExtensionControllerCalibrationData[WiiNunchuckCalibration_AccelerometerZ0GOffset])));

            OutputGeneric->ShoulderButtons.topLeft = !(WiiExtensionControllerData[WiiNunchuckControllerData_ButtonOffset] & WiiNunchuckButton_C);
            OutputGeneric->ShoulderButtons.bottomLeft = !(WiiExtensionControllerData[WiiNunchuckControllerData_ButtonOffset] & WiiNunchuckButton_Z);
            break;
    }
}
