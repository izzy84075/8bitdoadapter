#pragma once

#include <stdint.h>
#include "GenericController.h"

typedef struct {
    uint8_t dataByte1;  //Up through C, in the upper six bits
    uint8_t dataByte2;  //A and Start and some ID flags in the upper six bits
    uint8_t dataByte3;  //More ID bits
    uint8_t dataByte4;  //Z through Mode and some unused bits, in the upper six bits
} SG6_DATA_t;

void GenericControllerToSG6(GenericController* ControllerInput, SG6_DATA_t* SG6Output);