#pragma once

#include <stdint.h>
#include "GenericController.h"

typedef struct {
    uint8_t dataByte;
} NES_DATA_t;

void GenericControllerToNES(GenericController* ControllerInput, NES_DATA_t* NESOutput);