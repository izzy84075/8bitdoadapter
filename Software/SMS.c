#include "SMS.h"

void GenericControllerToSMS(GenericController* ControllerInput, SMS_DATA_t* SMSOutput) {
    //Up
    if(ControllerInput.DPad.up || (ControllerInput.LeftJoystick.Y > 64)) {
        SMSOutput.dataByte &= ~(0x80);
    } else {
        SMSOutput.dataByte |= 0x80;
    }
    //Down
    if(ControllerInput.DPad.down || (ControllerInput.LeftJoystick.Y < -64)) {
        SMSOutput.dataByte &= ~(0x40);
    } else {
        SMSOutput.dataByte |= 0x40;
    }
    //Left
    if(ControllerInput.DPad.left || (ControllerInput.LeftJoystick.X < -64)) {
        SMSOutput.dataByte &= ~(0x20);
    } else {
        SMSOutput.dataByte |= 0x20;
    }
    //Right
    if(ControllerInput.DPad.right || (ControllerInput.LeftJoystick.X > 64)) {
        SMSOutput.dataByte &= ~(0x10);
    } else {
        SMSOutput.dataByte |= 0x10;
    }
    //"1"
    if(ControllerInput.FaceButtons.bottomRight || ControllerInput.FaceButtons.topRight || ControllerInput.FaceButtons.bottomCenter) {
        SMSOutput.dataByte &= ~(0x08);
    } else {
        SMSOutput.dataByte |= 0x08;
    }
    //"2"
    if(ControllerInput.FaceButtons.bottomLeft || ControllerInput.FaceButtons.topLeft || ControllerInput.FaceButtons.topCenter) {
        SMSOutput.dataByte &= ~(0x04);
    } else {
        SMSOutput.dataByte |= 0x04;
    }
}