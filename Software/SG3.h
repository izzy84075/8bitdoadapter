#pragma once

#include <stdint.h>
#include "GenericController.h"

typedef struct {
    uint8_t dataByte1;  //Up through C, in the upper six bits
    uint8_t dataByte2;  //A and Start and some ID flags in the upper six bits
} SG3_DATA_t;

void GenericControllerToSG3(GenericController* ControllerInput, SG3_DATA_t* SG3Output);