#pragma once

#include <stdint.h>
#include "GenericController.h"

typedef struct {
    uint8_t dataByte1;  //A through D-pad Right
    uint8_t dataByte2;  //"Reset" through C-pad Right
    int8_t joystickX;
    int8_t joystickY;
} N64_DATA_t;

void GenericControllerToN64(GenericController* ControllerInput, N64_DATA_t* N64Output);