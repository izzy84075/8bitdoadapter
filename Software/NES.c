#include "NES.h"

void GenericControllerToNES(GenericController* ControllerInput, NES_DATA_t* NESOutput) {
    //B
    if(ControllerInput.FaceButtons.bottomLeft || ControllerInput.FaceButtons.topLeft || ControllerInput.FaceButtons.topCenter) {
        NESOutput.dataByte &= ~(0x80);
    } else {
        NESOutput.dataByte |= 0x80;
    }
    //A
    if(ControllerInput.FaceButtons.bottomRight || ControllerInput.FaceButtons.topRight || ControllerInput.FaceButtons.bottomCenter) {
        NESOutput.dataByte &= ~(0x40);
    } else {
        NESOutput.dataByte |= 0x40;
    }
    //Select
    if(ControllerInput.MiscButtons.bit1) {
        NESOutput.dataByte &= ~(0x20);
    } else {
        NESOutput.dataByte |= 0x20;
    }
    //Start
    if(ControllerInput.MiscButtons.bit0) {
        NESOutput.dataByte &= ~(0x10);
    } else {
        NESOutput.dataByte |= 0x10;
    }
    //Up
    if(ControllerInput.DPad.up || (ControllerInput.LeftJoystick.Y > 64)) {
        NESOutput.dataByte &= ~(0x08);
    } else {
        NESOutput.dataByte |= 0x08;
    }
    //Down
    if(ControllerInput.DPad.down || (ControllerInput.LeftJoystick.Y < -64)) {
        NESOutput.dataByte &= ~(0x04);
    } else {
        NESOutput.dataByte |= 0x04;
    }
    //Left
    if(ControllerInput.DPad.left || (ControllerInput.LeftJoystick.X < -64)) {
        NESOutput.dataByte &= ~(0x02);
    } else {
        NESOutput.dataByte |= 0x02;
    }
    //Right
    if(ControllerInput.DPad.right || (ControllerInput.LeftJoystick.X > 64)) {
        NESOutput.dataByte &= ~(0x01);
    } else {
        NESOutput.dataByte |= 0x01;
    }
}
