#include "SS3D.h"

void GenericControllerToSS3D(GenericController* ControllerInput, SS3D_DATA_t* SS3DOutput) {
    //Right
    if(ControllerInput.DPad.right) {
        SS3DOutput.dataByte1 &= ~(0x80);
    } else {
        SS3DOutput.dataByte1 |= 0x80;
    }
    //Left
    if(ControllerInput.DPad.left) {
        SS3DOutput.dataByte1 &= ~(0x40);
    } else {
        SS3DOutput.dataByte1 |= 0x40;
    }
    //Down
    if(ControllerInput.DPad.down) {
        SS3DOutput.dataByte1 &= ~(0x20);
    } else {
        SS3DOutput.dataByte1 |= 0x20;
    }
    //Up
    if(ControllerInput.DPad.up) {
        SS3DOutput.dataByte1 &= ~(0x10);
    } else {
        SS3DOutput.dataByte1 |= 0x10;
    }
    
    //Start
    if(ControllerInput.MiscButtons.bit0) {
        SS3DOutput.dataByte2 &= ~(0x80);
    } else {
        SS3DOutput.dataByte2 |= 0x80;
    }
    //A
    if(ControllerInput.FaceButtons.bottomLeft) {
        SS3DOutput.dataByte2 &= ~(0x40);
    } else {
        SS3DOutput.dataByte2 |= 0x40;
    }
    //C
    if(ControllerInput.FaceButtons.bottomRight) {
        SS3DOutput.dataByte2 &= ~(0x20);
    } else {
        SS3DOutput.dataByte2 |= 0x20;
    }
    //B
    if(ControllerInput.FaceButtons.bottomCenter) {
        SS3DOutput.dataByte2 &= ~(0x10);
    } else {
        SS3DOutput.dataByte2 |= 0x10;
    }
    
    //R
    if(ControllerInput.ShoulderButtons.topRight || ControllerInput.ShoulderButtons.bottomRight) {
        SS3DOutput.dataByte3 &= ~(0x80);
    } else {
        SS3DOutput.dataByte3 |= 0x80;
    }
    //X
    if(ControllerInput.FaceButtons.topLeft) {
        SS3DOutput.dataByte3 &= ~(0x40);
    } else {
        SS3DOutput.dataByte3 |= 0x40;
    }
    //Z
    if(ControllerInput.FaceButtons.topRight) {
        SS3DOutput.dataByte3 &= ~(0x20);
    } else {
        SS3DOutput.dataByte3 |= 0x20;
    }
    //Y
    if(ControllerInput.FaceButtons.topCenter) {
        SS3DOutput.dataByte3 &= ~(0x10);
    } else {
        SS3DOutput.dataByte3 |= 0x10;
    }
    
    //L
    if(ControllerInput.ShoulderButtons.topLeft || ControllerInput.ShoulderButtons.bottomLeft) {
        SS3DOutput.dataByte4 &= ~(0x80);
    } else {
        SS3DOutput.dataByte4 |= 0x80;
    }
    //ID flags
    SS3DOutput.dataByte4 |= 0x70;

    //Analog stuff
    SS3DOutput.joystickX = (uint8_t)((int16_t)(ControllerInput.LeftJoystick.X) + 128);
    SS3DOutput.joystickY = (uint8_t)((int16_t)(ControllerInput.LeftJoystick.Y) + 128);
    SS3DOutput.rightTrigger = (uint8_t)((ControllerInput.MiscAnalog[0] >> 8));
    SS3DOutput.leftTrigger = (uint8_t)((ControllerInput.MiscAnalog[1] >> 8));
}