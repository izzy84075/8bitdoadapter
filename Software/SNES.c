#include "SNES.h"

void GenericControllerToSNES(GenericController* ControllerInput, SNES_DATA_t* SNESOutput) {
    //B
    if(ControllerInput.FaceButtons.bottomLeft) {
        SNESOutput.dataByte1 &= ~(0x80);
    } else {
        SNESOutput.dataByte1 |= 0x80;
    }
    //Y
    if(ControllerInput.FaceButtons.topLeft) {
        SNESOutput.dataByte1 &= ~(0x40);
    } else {
        SNESOutput.dataByte1 |= 0x40;
    }
    //Select
    if(ControllerInput.MiscButtons.bit1) {
        SNESOutput.dataByte1 &= ~(0x20);
    } else {
        SNESOutput.dataByte1 |= 0x20;
    }
    //Start
    if(ControllerInput.MiscButtons.bit0) {
        SNESOutput.dataByte1 &= ~(0x10);
    } else {
        SNESOutput.dataByte1 |= 0x10;
    }
    //Up
    if(ControllerInput.DPad.up || (ControllerInput.LeftJoystick.Y > 64)) {
        SNESOutput.dataByte1 &= ~(0x08);
    } else {
        SNESOutput.dataByte1 |= 0x08;
    }
    //Down
    if(ControllerInput.DPad.down || (ControllerInput.LeftJoystick.Y < -64)) {
        SNESOutput.dataByte1 &= ~(0x04);
    } else {
        SNESOutput.dataByte1 |= 0x04;
    }
    //Left
    if(ControllerInput.DPad.left || (ControllerInput.LeftJoystick.X < -64)) {
        SNESOutput.dataByte1 &= ~(0x02);
    } else {
        SNESOutput.dataByte1 |= 0x02;
    }
    //Right
    if(ControllerInput.DPad.right || (ControllerInput.LeftJoystick.X > 64)) {
        SNESOutput.dataByte1 &= ~(0x01);
    } else {
        SNESOutput.dataByte1 |= 0x01;
    }
    //A
    if(ControllerInput.FaceButtons.bottomRight || ControllerInput.FaceButtons.bottomCenter) {
        SNESOutput.dataByte2 &= ~(0x80);
    } else {
        SNESOutput.dataByte2 |= 0x80;
    }
    //X
    if(ControllerInput.FaceButtons.topRight || ControllerInput.FaceButtons.topCenter) {
        SNESOutput.dataByte1 &= ~(0x40);
    } else {
        SNESOutput.dataByte1 |= 0x40;
    }
    //L
    if(ControllerInput.ShoulderButtons.topLeft || ControllerInput.ShoulderButtons.bottomRight) {
        SNESOutput.dataByte1 &= ~(0x20);
    } else {
        SNESOutput.dataByte1 |= 0x20;
    }
    //R
    if(ControllerInput.ShoulderButtons.topRight || ControllerInput.ShoulderButtons.bottomRight) {
        SNESOutput.dataByte1 &= ~(0x10);
    } else {
        SNESOutput.dataByte1 |= 0x10;
    }
}