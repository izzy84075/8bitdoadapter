#include "SDC.h"

void GenericControllerToSDC(GenericController* ControllerInput, SDC_DATA_t* SDCOutput) {
    //D-Pad 2
    //Don't actually have any buttons to map here...
    //Can't find any controllers that used it anyways.
    SDCOutput.dataWord |= 0xF000;
    //"D button", let's map Select to it...
    if(ControllerInput.MiscButtons.bit1) {
        SDCOutput.dataWord &= ~(0x0800);
    } else {
        SDCOutput.dataWord |= 0x0800;
    }
    //X
    if(ControllerInput.FaceButtons.topLeft) {
        SDCOutput.dataWord &= ~(0x0400);
    } else {
        SDCOutput.dataWord |= 0x0400;
    }
    //Y
    if(ControllerInput.FaceButtons.topCenter) {
        SDCOutput.dataWord &= ~(0x0200);
    } else {
        SDCOutput.dataWord |= 0x0200;
    }
    //Z
    if(ControllerInput.FaceButtons.topRight) {
        SDCOutput.dataWord &= ~(0x0100);
    } else {
        SDCOutput.dataWord |= 0x0100;
    }
    //Right
    if(ControllerInput.DPad.right) {
        SDCOutput.dataWord &= ~(0x0080);
    } else {
        SDCOutput.dataWord |= 0x0080;
    }
    //Left
    if(ControllerInput.DPad.left) {
        SDCOutput.dataWord &= ~(0x0040);
    } else {
        SDCOutput.dataWord |= 0x0040;
    }
    //Down
    if(ControllerInput.DPad.down) {
        SDCOutput.dataWord &= ~(0x0020);
    } else {
        SDCOutput.dataWord |= 0x0020;
    }
    //Up
    if(ControllerInput.DPad.up) {
        SDCOutput.dataWord &= ~(0x0010);
    } else {
        SDCOutput.dataWord |= 0x0010;
    }
    //Start
    if(ControllerInput.MiscButtons.bit0) {
        SDCOutput.dataWord &= ~(0x0008);
    } else {
        SDCOutput.dataWord |= 0x0008;
    }
    //A
    if(ControllerInput.FaceButtons.bottomLeft) {
        SDCOutput.dataWord &= ~(0x0004);
    } else {
        SDCOutput.dataWord |= 0x0004;
    }
    //B
    if(ControllerInput.FaceButtons.bottomCenter) {
        SDCOutput.dataWord &= ~(0x0002);
    } else {
        SDCOutput.dataWord |= 0x0002;
    }
    //C
    if(ControllerInput.FaceButtons.bottomRight) {
        SDCOutput.dataWord &= ~(0x0001);
    } else {
        SDCOutput.dataWord |= 0x0001;
    }

    //Analog stuff
    SDCOutput.rightTrigger = (uint8_t)((ControllerInput.MiscAnalog[0] >> 8));
    SDCOutput.leftTrigger = (uint8_t)((ControllerInput.MiscAnalog[1] >> 8));
    SDCOutput.joystickX = (uint8_t)((int16_t)(ControllerInput.LeftJoystick.X) + 128);
    SDCOutput.joystickY = (uint8_t)((int16_t)(ControllerInput.LeftJoystick.Y) + 128);
    SDCOutput.joystick2X = (uint8_t)((int16_t)(ControllerInput.RightJoystick.X) + 128);
    SDCOutput.joystick2Y = (uint8_t)((int16_t)(ControllerInput.RightJoystick.Y) + 128);
}