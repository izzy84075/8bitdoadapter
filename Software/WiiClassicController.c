#include "WiiNunchuck.h"

#define WiiClassicControllerButton_DPadRight            0x80
#define WiiClassicControllerButton_DPadDown             0x40
#define WiiClassicControllerButton_LeftTrigger          0x20
#define WiiClassicControllerButton_MinusButton          0x10
#define WiiClassicControllerButton_HomeButton           0x08
#define WiiClassicControllerButton_PlusButton           0x04
#define WiiClassicControllerButton_RightTrigger         0x02

#define WiiClassicControllerButton_ZLButton             0x80
#define WiiClassicControllerButton_BButton              0x40
#define WiiClassicControllerButton_YButton              0x20
#define WiiClassicControllerButton_AButton              0x10
#define WiiClassicControllerButton_XButton              0x08
#define WiiClassicControllerButton_ZRButton             0x04
#define WiiClassicControllerButton_DPadLeft             0x02
#define WiiClassicControllerButton_DPadUp               0x01


#define WiiClassicControllerCalibration_LeftJoystickXMaxOffset       0
#define WiiClassicControllerCalibration_LeftJoystickXMinOffset       1
#define WiiClassicControllerCalibration_LeftJoystickXCenterOffset    2
#define WiiClassicControllerCalibration_LeftJoystickYMaxOffset       3
#define WiiClassicControllerCalibration_LeftJoystickYMinOffset       4
#define WiiClassicControllerCalibration_LeftJoystickYCenterOffset    5
#define WiiClassicControllerCalibration_RightJoystickXMaxOffset      6
#define WiiClassicControllerCalibration_RightJoystickXMinOffset      7
#define WiiClassicControllerCalibration_RightJoystickXCenterOffset   8
#define WiiClassicControllerCalibration_RightJoystickYMaxOffset      9
#define WiiClassicControllerCalibration_RightJoystickYMinOffset      10
#define WiiClassicControllerCalibration_RightJoystickYCenterOffset   11
#define WiiClassicControllerCalibration_LeftTriggerMinOffset         12
#define WiiClassicControllerCalibration_LeftTriggerMaxOffset         13
#define WiiClassicControllerCalibration_RightTriggerMinOffset        14
#define WiiClassicControllerCalibration_RightTriggerMaxOffset        15

void WiiClassicControllerToGeneric(uint8_t[] WiiExtensionControllerData, uint8_t[] WiiExtensionControllerCalibrationData, GENERIC_CONTROLLER_t* OutputGeneric) {
    uint8_t leftJoystickXValue;
    uint8_t leftJoystickYValue;
    uint8_t rightJoystickXValue;
    uint8_t rightJoystickYValue;
    uint8_t leftTriggerValue;
    uint8_t rightTriggerValue;

    leftJoystickXValue = WiiExtensionControllerData[0] & 0x3F;
    leftJoystickYValue = WiiExtensionControllerData[1] & 0x3F;
    rightJoystickYValue = WiiExtensionControllerData[2] & 0x1F;
    rightTriggerValue = WiiExtensionControllerData[3] & 0x1F;

    rightJoystickXValue = ((WiiExtensionControllerData[0] & 0xC0) >> 3) | ((WiiExtensionControllerData[1] & 0xC0) >> 5) | ((WiiExtensionControllerData[2] & 0x80) >> 7);
    leftTriggerValue = ((WiiExtensionControllerData[2] & 0x60) >> 2) | ((WiiExtensionControllerData[3] & 0xE0) >> 5);

    if(WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftJoystickXMaxOffset] != 0x00) {
        OutputGeneric.LeftJoystick.X = (int8_t)(((((float)(leftJoystickXValue) - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftJoystickXCenterOffset]) /  (WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftJoystickXMaxOffset] - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftJoystickXMinOffset]))*127));
    } else {
        OutputGeneric.LeftJoystick.X = (int8_t)(leftJoystickXValue) - 128;
    }
    if(WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftJoystickYMaxOffset] != 0x00) {
        OutputGeneric.LeftJoystick.Y = (int8_t)(((((float)(leftJoystickYValue) - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftJoystickYCenterOffset]) /  (WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftJoystickYMaxOffset] - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftJoystickYMinOffset]))*127));
    } else {
        OutputGeneric.LeftJoystick.Y = (int8_t)(leftJoystickYValue) - 128;
    }

    if(WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightJoystickXMaxOffset] != 0x00) {
        OutputGeneric.RightJoystick.X = (int8_t)(((((float)(rightJoystickXValue) - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightJoystickXCenterOffset]) /  (WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightJoystickXMaxOffset] - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightJoystickXMinOffset]))*127));
    } else {
        OutputGeneric.RightJoystick.X = (int8_t)(rightJoystickXValue) - 128;
    }
    if(WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightJoystickYMaxOffset] != 0x00) {
        OutputGeneric.RightJoystick.Y = (int8_t)(((((float)(rightJoystickYValue) - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightJoystickYCenterOffset]) /  (WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightJoystickYMaxOffset] - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightJoystickYMinOffset]))*127));
    } else {
        OutputGeneric.RightJoystick.Y = (int8_t)(rightJoystickYValue) - 128;
    }

    if(WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightTriggerMaxOffset] != 0x00) {
        OutputGeneric.MiscAnalog[0] = (int16_t)(((((float)(rightTriggerValue) - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightTriggerMinOffset]) /  (WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightTriggerMaxOffset] - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_RightTriggerMinOffset]))*65535));
    } else {
        OutputGeneric.MiscAnalog[0] = rightTriggerValue * 2114;
    }

    if(WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftTriggerMaxOffset] != 0x00) {
        OutputGeneric.MiscAnalog[1] = (int16_t)(((((float)(leftTriggerValue) - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftTriggerMinOffset]) /  (WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftTriggerMaxOffset] - WiiExtensionControllerCalibrationData[WiiClassicControllerCalibration_LeftTriggerMinOffset]))*65535));
    } else {
        OutputGeneric.MiscAnalog[1] = leftTriggerValue * 2114;
    }

    OutputGeneric.DPad.up = ((WiiExtensionControllerData[5] & WiiClassicControllerButton_DPadUp) != WiiClassicControllerButton_DPadUp);
    OutputGeneric.DPad.down = ((WiiExtensionControllerData[4] & WiiClassicControllerButton_DPadDown) != WiiClassicControllerButton_DPadDown);
    OutputGeneric.DPad.left = ((WiiExtensionControllerData[5] & WiiClassicControllerButton_DPadLeft) != WiiClassicControllerButton_DPadLeft);
    OutputGeneric.DPad.right = ((WiiExtensionControllerData[4] & WiiClassicControllerButton_DPadRight) != WiiClassicControllerButton_DPadRight);

    OutputGeneric.FaceButtons.topRight = ((WiiExtensionControllerData[5] & WiiClassicControllerButton_XButton) != WiiClassicControllerButton_XButton);
    OutputGeneric.FaceButtons.bottomLeft = ((WiiExtensionControllerData[5] & WiiClassicControllerButton_BButton) != WiiClassicControllerButton_BButton);
    OutputGeneric.FaceButtons.topLeft = ((WiiExtensionControllerData[5] & WiiClassicControllerButton_YButton) != WiiClassicControllerButton_YButton);
    OutputGeneric.FaceButtons.bottomRight = ((WiiExtensionControllerData[5] & WiiClassicControllerButton_AButton) != WiiClassicControllerButton_AButton);

    OutputGeneric.ShoulderButtons.topLeft = ((WiiExtensionControllerData[4] & WiiClassicControllerButton_LeftTrigger) != WiiClassicControllerButton_LeftTrigger);
    OutputGeneric.ShoudlerButtons.topRight = ((WiiExtensionControllerData[4] & WiiClassicControllerButton_RightTrigger) != WiiClassicControllerButton_RightTrigger);
    OutputGeneric.ShoulderButtons.bottomLeft = ((WiiExtensionControllerData[5] & WiiClassicControllerButton_ZLButton) != WiiClassicControllerButton_ZLButton);
    OutputGeneric.ShoulderButtons.bottomRight = ((WiiExtensionControllerData[5] & WiiClassicControllerButton_ZRButton) != WiiClassicControllerButton_ZRButton);

    OutputGeneric.MiscButtons.bit0 = ((WiiExtensionControllerData[4] & WiiClassicControllerButton_PlusButton) != WiiClassicControllerButton_PlusButton);
    OutputGeneric.MiscButtons.bit1 = ((WiiExtensionControllerData[4] & WiiClassicControllerButton_MinusButton) != WiiClassicControllerButton_MinusButton);
    OutputGeneric.MiscButtons.bit2 = ((WiiExtensionControllerData[4] & WiiClassicControllerButton_HomeButton) != WiiClassicControllerButton_HomeButton);
}