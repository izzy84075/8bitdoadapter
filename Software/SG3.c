#include "SG3.h"

void GenericControllerToSG3(GenericController* ControllerInput, SG3_DATA_t* SG3Output) {
    //Up
    if(ControllerInput.DPad.up || (ControllerInput.LeftJoystick.Y > 64)) {
        SG3Output.dataByte1 &= ~(0x80);
    } else {
        SG3Output.dataByte1 |= 0x80;
    }
    //Down
    if(ControllerInput.DPad.down || (ControllerInput.LeftJoystick.Y < -64)) {
        SG3Output.dataByte1 &= ~(0x40);
    } else {
        SG3Output.dataByte1 |= 0x40;
    }
    //Left
    if(ControllerInput.DPad.left || (ControllerInput.LeftJoystick.X < -64)) {
        SG3Output.dataByte1 &= ~(0x20);
    } else {
        SG3Output.dataByte1 |= 0x20;
    }
    //Right
    if(ControllerInput.DPad.right || (ControllerInput.LeftJoystick.X > 64)) {
        SG3Output.dataByte1 &= ~(0x10);
    } else {
        SG3Output.dataByte1 |= 0x10;
    }
    //B
    if(ControllerInput.FaceButtons.bottomCenter || ControllerInput.FaceButtons.topCenter) {
        SG3Output.dataByte1 &= ~(0x08);
    } else {
        SG3Output.dataByte1 |= 0x08;
    }
    //C
    if(ControllerInput.FaceButtons.bottomRight || ControllerInput.FaceButtons.topRight) {
        SG3Output.dataByte1 &= ~(0x04);
    } else {
        SG3Output.dataByte1 |= 0x04;
    }

    //ID flags
    SG3Output.dataByte2 |= 0xC0;
    SG3Output.dataByte2 &= ~(0x30);
    //A
    if(ControllerInput.FaceButtons.bottomLeft || ControllerInput.FaceButtons.topLeft) {
        SG3Output.dataByte2 &= ~(0x08);
    } else {
        SG3Output.dataByte2 |= 0x08;
    }
    //Start
    if(ControllerInput.MiscButtons.bit0) {
        SG3Output.dataByte2 &= ~(0x04);
    } else {
        SG3Output.dataByte2 |= 0x04;
    }
}