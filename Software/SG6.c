#include "SG6.h"

void GenericControllerToSG6(GenericController* ControllerInput, SG6_DATA_t* SG6Output) {
    //Up
    if(ControllerInput.DPad.up || (ControllerInput.LeftJoystick.Y > 64)) {
        SG6Output.dataByte1 &= ~(0x80);
    } else {
        SG6Output.dataByte1 |= 0x80;
    }
    //Down
    if(ControllerInput.DPad.down || (ControllerInput.LeftJoystick.Y < -64)) {
        SG6Output.dataByte1 &= ~(0x40);
    } else {
        SG6Output.dataByte1 |= 0x40;
    }
    //Left
    if(ControllerInput.DPad.left || (ControllerInput.LeftJoystick.X < -64)) {
        SG6Output.dataByte1 &= ~(0x20);
    } else {
        SG6Output.dataByte1 |= 0x20;
    }
    //Right
    if(ControllerInput.DPad.right || (ControllerInput.LeftJoystick.X > 64)) {
        SG6Output.dataByte1 &= ~(0x10);
    } else {
        SG6Output.dataByte1 |= 0x10;
    }
    //B
    if(ControllerInput.FaceButtons.bottomCenter) {
        SG6Output.dataByte1 &= ~(0x08);
    } else {
        SG6Output.dataByte1 |= 0x08;
    }
    //C
    if(ControllerInput.FaceButtons.bottomRight) {
        SG6Output.dataByte1 &= ~(0x04);
    } else {
        SG6Output.dataByte1 |= 0x04;
    }

    //ID flags
    SG6Output.dataByte2 |= 0xC0;
    SG6Output.dataByte2 &= ~(0x30);
    //A
    if(ControllerInput.FaceButtons.bottomLeft) {
        SG6Output.dataByte2 &= ~(0x08);
    } else {
        SG6Output.dataByte2 |= 0x08;
    }
    //Start
    if(ControllerInput.MiscButtons.bit0) {
        SG6Output.dataByte2 &= ~(0x04);
    } else {
        SG6Output.dataByte2 |= 0x04;
    }

    //ID flags
    SG6Output.dataByte3 = 0x0C;

    //Z
    if(ControllerInput.FaceButtons.topRight) {
        SG6Output.dataByte4 &= ~(0x80);
    } else {
        SG6Output.dataByte4 |= 0x80;
    }
    //Y
    if(ControllerInput.FaceButtons.topCenter) {
        SG6Output.dataByte4 &= ~(0x40);
    } else {
        SG6Output.dataByte4 |= 0x40;
    }
    //X
    if(ControllerInput.FaceButtons.topLeft) {
        SG6Output.dataByte4 &= ~(0x20);
    } else {
        SG6Output.dataByte4 |= 0x20;
    }
    //Mode
    if(ControllerInput.ShoulderButtons.topLeft || ControllerInput.ShoulderButtons.topRight || ControllerInput.ShoulderButtons.bottomLeft || ControllerInput.ShoulderButtons.bottomRight) {
        SG6Output.dataByte4 &= ~(0x10);
    } else {
        SG6Output.dataByte4 |= 0x10;
    }
    //Unused bits
    SG6Output.dataByte4 |= 0x0C;
}