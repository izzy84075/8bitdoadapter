#include "SS6.h"

void GenericControllerToSS6(GenericController* ControllerInput, SS6_DATA_t* SS6Output) {
    //R
    if(ControllerInput.ShoulderButtons.topRight || ControllerInput.ShoulderButtons.bottomRight) {
        SS6Output.dataByte1 &= ~(0x80);
    } else {
        SS6Output.dataByte1 |= 0x80;
    }
    //X
    if(ControllerInput.FaceButtons.topLeft) {
        SS6Output.dataByte1 &= ~(0x40);
    } else {
        SS6Output.dataByte1 |= 0x40;
    }
    //Z
    if(ControllerInput.FaceButtons.topRight) {
        SS6Output.dataByte1 &= ~(0x20);
    } else {
        SS6Output.dataByte1 |= 0x20;
    }
    //Y
    if(ControllerInput.FaceButtons.topCenter) {
        SS6Output.dataByte1 &= ~(0x10);
    } else {
        SS6Output.dataByte1 |= 0x10;
    }
    
    //Start
    if(ControllerInput.MiscButtons.bit0) {
        SS6Output.dataByte2 &= ~(0x80);
    } else {
        SS6Output.dataByte2 |= 0x80;
    }
    //A
    if(ControllerInput.FaceButtons.bottomLeft) {
        SS6Output.dataByte2 &= ~(0x40);
    } else {
        SS6Output.dataByte2 |= 0x40;
    }
    //C
    if(ControllerInput.FaceButtons.bottomRight) {
        SS6Output.dataByte2 &= ~(0x20);
    } else {
        SS6Output.dataByte2 |= 0x20;
    }
    //B
    if(ControllerInput.FaceButtons.bottomCenter) {
        SS6Output.dataByte2 &= ~(0x10);
    } else {
        SS6Output.dataByte2 |= 0x10;
    }
    
    //Right
    if(ControllerInput.DPad.right || (ControllerInput.LeftJoystick.X > 64)) {
        SS6Output.dataByte3 &= ~(0x80);
    } else {
        SS6Output.dataByte3 |= 0x80;
    }
    //Left
    if(ControllerInput.DPad.left || (ControllerInput.LeftJoystick.X < -64)) {
        SS6Output.dataByte3 &= ~(0x40);
    } else {
        SS6Output.dataByte3 |= 0x40;
    }
    //Down
    if(ControllerInput.DPad.down || (ControllerInput.LeftJoystick.Y < -64)) {
        SS6Output.dataByte3 &= ~(0x20);
    } else {
        SS6Output.dataByte3 |= 0x20;
    }
    //Up
    if(ControllerInput.DPad.up || (ControllerInput.LeftJoystick.Y > 64)) {
        SS6Output.dataByte3 &= ~(0x10);
    } else {
        SS6Output.dataByte3 |= 0x10;
    }
    
    //L
    if(ControllerInput.ShoulderButtons.topLeft || ControllerInput.ShoulderButtons.bottomLeft) {
        SS6Output.dataByte4 &= ~(0x80);
    } else {
        SS6Output.dataByte4 |= 0x80;
    }
    //ID flags
    SS6Output.dataByte4 |= 0x40;
    SS6Output.dataByte4 &= ~(0x30);
}