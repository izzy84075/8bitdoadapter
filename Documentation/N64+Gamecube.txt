The Nintendo 64 and Nintendo Gamecube share a core communications protocol, officially called the "Joybus Protocol". The Gameboy Advance can also speak this protocol, when using the "GameCube - Game Boy Advance link cable", to allow for Gamecube games to interact with Gameboy Advances and GAmeboy Advance games.

This is a timing-based protocol, similar to what would be found in an IR-based system, and shares a bidirectional open-collector data line between the console and accessories.

Accessories should include a pullup resistor of roughly 760 Ohm on the data line to allow for reliable data transfer.

As the data line is open-collector, data transfer consists of pulling the line "Low" for varying periods, and then letting the line be pulled back "high" by the pullup resistors.

There are four distinct signal patterns used in this protocol:
    Zero:
        A Zero bit is transferred by pulling the line low for 3 microseconds, and then releasing the line for 1 microseconds.
    One:
        A One bit is transferred by pulling the line low for 1 microsecond, and then releasing the line for 3 microseconds.
    Console Stop:
        The Console signals the end of a transmission by pulling the line low for 1 microsecond, and then releasing the line for 2 microseconds.
    Accessory Stop:
        The Acessory signals the end of a transmission by pulling the line low for 2 microseconds, and then releasing the line for 2 microseconds.

Commands/responses are of fixed length, so the ambiguity of the Console's Stop bit and the One bit is not necessarily problematic, and more of a "politeness" thing.

Bits are concatenated into bytes, and bytes concatenated into commands/responses. A command or response should be terminated by a single Stop bit of the appropriate type.

IMPORTANT: Though I cannot find any documentation saying which endianness the protocol "officially" uses, this document is written using "big endian, most-significant byte first, most-significant bit first", and if you send the data in that order, it will match the on-wire protocol as far as I can tell.

Accessories DO NOT initiate communications, they only reply to commands by the console, and should reply as soon as possible after the console's Stop bit.

There are roughly 18 "known" command/response sequences, and a few handfuls of "seen, but not understood" command/response sequences, that handle all N64, Gamecube, and Gameboy Advance accessory interactions.
Most of these command/response sequences are specific to either the N64 or the Gamecube/GBA, but an Acessory can support all of them, as long as it identifies itself to the console as something the game on the console knows how to handle.

N64/Gamecube/GBA commands:
    ACCESSORY-INFO:
        Used to initialize and identify Accessories.
        
        Console sends 1 byte, 0x00.
        
        Accessory should reply with 3 bytes.
            The first two bytes are a 16-bit ID, identifying what kind of accessory it is.
            The third byte seems to be status flags, showing whether an N64 Controller Pak is present or not, and whether the GCN controller's rumble motor is on or off.
        
        KNOWN ACCESSORY IDs:
            0x0080      N64 Cartridge EEPROM (Status byte will be 0x00)
            0x0500      N64 Standard Controller
            0x0001      N64 Voice Recognition Unit
            0x0200      N64 Mouse
            0x0002      N64 Keyboard
            0x2004      N64 "Densha de Go 64" Train Controller
            
            0x0004      GBA
            
            0x0900      GCN Standard Controller
            0x8800      GCN Wavebird Receiver with no controller connected
            0x8B10      GCN Wavebird Receiver with controller connected
            0x0800      GCN Steering Wheel
            0x0820      GCN Keyboard
            
            All GCN-compatible controllers seem to have bit 11 set, as a way of distinguishing them from an N64 controller.
            Bit 8 seems to be a "GCN standard controller compatible" marker.
            Bit 16 seems to be a "Wireless controller" marker.
        
        STATUS BYTE FLAGS:
            0x08: GCN Rumble Motor on
            0x04: N64 Controller Pak CRC error
            0x02: N64 No Controller Pak present
            0x01: N64 Controller Pak present
    
    ACESSORY-RESET-AND-INFO:
        Used to reset and initialize Accessories.
        
        Console sends 1 byte, 0xFF.
        
        Accessory should treat this as a "reset" signal, and then reply with it's ID and status, as in ACCESSORY-INFO.

N64 commands:
    CONTROLLER-STATE:
        Used to read user inputs from Accessories.
        
        Console sends 1 byte, 0x01.
        
        Accessory should reply with 4 bytes, containing the button states and joystick position.
        
        Button states are that a "1" is pressed, and a "0" is released.
        
        Byte 0:
            Bit 7: A button
            Bit 6: B button
            Bit 5: Z button
            Bit 4: Start button
            Bit 3: D-pad Up
            Bit 2: D-pad Down
            Bit 1: D-pad Left
            Bit 0: D-pad Right
        Byte 1:
            Bit 7: "Reset" pressed (Left Trigger, Right Trigger, and Start all pressed at the same time)
            Bit 6: Unused?
            Bit 5: Left Trigger
            Bit 4: Right Trigger
            Bit 3: C-pad Up
            Bit 2: C-pad Down
            Bit 1: C-pad Left
            Bit 0: C-pad Right
        Byte 2:
            X-axis position in Signed 8-bit format, range -50(Left) to +50(Right).
        Byte 3:
            Y-axis position in Signed 8-bit format, range -50(Down) to +50(Up).
    READ-FROM-CONTROLLER-PAK:
        Used to read data from Controller Paks.
        
        Console sends 3 bytes: 0x02, 0bAAAAAAAA, 0bAAABBBBB.
        
        The bits labeled AAAAAAAAAAA are the upper 11 bits of the 16-bit address to read. The low 5 bits are assumed to be "0", as all reads/writes are in 32-byte blocks.
        The bits labeled BBBBB are a checksum of the address, documented in N64-CRC.txt as "Address Checksum".
        
        The Accessory should then reply with 32 bytes of data from the specified address, followed by a "Data Checksum", also documented in N64-CRC.txt.
        
        See N64-ControllerPak-MemoryMap.txt for the memory maps of various accessories.
    WRITE-TO-CONTROLLER-PAK:
        Used to write data to Controller Paks.
        
        Console sends 35 bytes: 0x03, 0bAAAAAAAA, 0bAAABBBBB, and 32 arbitrary bytes.
        
        The address and address checksum match the READ-FROM-CONTROLLER-PAK command, and the 32 bytes are to be written to the given address.
        
        See N64-ControllerPak-MemoryMap.txt for the memory maps of various accessories.
        
        The Accessory should respond with a single byte, with the Data Checksum of the written data.
    
    NOTE: The following commands are "internal" to the N64, and documented here only for completion's sake.
    
    READ-FROM-EEPROM:
        Used to read data from a cartridge's internal EEPROM.
        
        Console sends 2 bytes: 0x04, 0xAA
        
        AA is an offset into the EEPROM, in 8-byte increments.
        
        The EEPROM will send back 8 bytes of data from the offset specified by AA.
    WRITE-TO-EEPROM:
        DOCUMENTATION INCOMPLETE
        
        Used to write data to a cartridge's internal EEPROM.
        
        Console sends 10 bytes: 0x05, 0xAA, and 8 data bytes.
        
        AA is an offset into the EEPROM, in 8-byte increments.
        
        The EEPROM will send back a single byte, of either 0x00 for a success, or <UNKNOWN> for an error.
    READ-RTC-STATUS:
        DOCUMENTATION INCOMPLETE
        
        Used to read the status of a cartridge's Real Time Clock. Only known to be used for Animal Forest.
        
        Console will send at least 1 byte, 0x06.
        
        It is unknown what the response is.
    READ-RTC-DATA:
        DOCUMENTATION INCOMPLETE
        
        Used to read the data from a cartridge's Real Time Clock. Only known to be used for Animal Forest.
        
        Console will send at least 1 byte, 0x07.
        
        It is unknown what the response is.
    WRITE-RTC-DATA:
        DOCUMENTATION INCOMPLETE
        
        Used to write data to a cartridge's Real Time Clock. Only known to be used for Animal Forest.
        
        Console will send at least 1 byte, 0x08.
        
        It is unknown what the response is.
    
    Commands 0x09 to 0x0D are known to be used for the Voice Recognition Unit/Voice Recognition System, but their exact meanings are unknown.
        DOCUMENTATION INCOMPLETE
    
    Commands 0x0E to 0x12 are not known to be used for anything.
    
    READ-RANDNET-KEYBOARD:
        Used to read keypresses from the RandNet keyboard.
        
        Console will send 2 bytes: 0x13, 0b00000ABC.
        
        A turns the Power LED on when set, B turns the Capslock LED on, and C turns the Numlock LED on.
        
        Accessory will reply with 7 bytes: 3 2-byte button codes, and 1 status byte.
        
        The button codes can be found in the N64-KeyboardCodes.htm document.
        
        The Status byte has two flags in it:
            0x10:   Too many keys pressed. If any more than 3 keys are pressed, NO keypresses will be reported, only this flag.
            0x01:   Home Key pressed.

Gamecube/GBA commands:
    READ-FROM-GBA:
        DOCUMENTATION INCOMPLETE
        
        Used to read data from a Gameboy Advance.
        
        Console will send 1 bytes, 0x14.
        
        Accessory will reply with 5 bytes: 4 data bytes, and one status byte. The contents of the status byte are unknown.
    WRITE-TO-GBA:
        DOCUMENTATION INCOMPLETE
        
        Used to write data to a Gameboy Advance.
        
        Console will send 5 bytes: 0x15, and 4 data bytes.
        
        Accessory will reply with 1 status byte. The contents of the status byte are unknown.
    
    Commands 0x16 to 0x2F are not known to be used for anything.
    
    FORCE-FEEDBACK:
        DOCUMENTATION INCOMPLETE
        
        Presumably used to send force feedback data to the steering wheel controller.
        
        Console will send at least one byte: 0x30.
    
    Commands 0x31 to 0x3F are not known to be used for anything.
    
    CONTROLLER-STATUS-AND-RUMBLE:
        DOCUMENTATION INCOMPLETE, double-check the function of the AA bits
        
        Used for reading controller button states and analog positions.
        
        Console will send 3 bytes: 0x40, 0b000000AA, 0b0000001B.
        
        The bits marked AA tell the controller to reply with button states and analog positions, if set to 0b11.
        The bit marked B turns the controller's rumble motor on if it is set, and off if not.
        
        The controller will reply with 8 bytes of button states and analog info, if the AA bits are set, or 0 bytes, otherwise.
        
        Byte 0:
            Bit 7: Unknown, usually 0
            Bit 6: Unknown, usually 0
            Bit 5: Unknown, usually 0
            Bit 4: Start button
            Bit 3: Y button
            Bit 2: X button
            Bit 1: B button
            Bit 0: A button
        Byte 1:
            Bit 7: Unknown, usually 1
            Bit 6: L button
            Bit 5: R button
            Bit 4: Z button
            Bit 3: D-pad Up
            Bit 2: D-pad Down
            Bit 1: D-pad Right
            Bit 0: D-pad Left
        Byte 2:
            Joystick's X-axis in 8-bit unsigned format.
        Byte 3:
            Joystick's Y-axis in 8-bit unsigned format.
        Byte 4:
            C-Stick's X-axis in 8-bit unsigned format.
        Byte 5:
            C-Stick's Y-axis in 8-bit unsigned format.
        Byte 6:
            L button's analog position in 8-bit unsigned format.
        Byte 7:
            R button's analog position in 8-bit unsigned format.
    CONTROLLER-READ-ORIGINS:
        DOCUMENTATION INCOMPLETE
        
        Presumably used for reading the value that the controller read during startup/calibration, so a game could check for a "bad" calibration/controller?
        
        Console will send at least 1 byte: 0x41.
        
        Unknown what the proper response is.
    CONTROLLER-CALIBRATE:
        DOCUMENTATION INCOMPLETE
        
        Presumably used to tell the controller to recalibrate using the current position as the "center" position.
        
        Console will send at least 1 byte: 0x42.
        
        Unknown what the proper response is.
    CONTROLLER-LONG-POLL:
        DOCUMENTATION INCOMPLETE
        
        This is referred to in a single project that was using a Gameboy Advance as a Gamecube controller, and nobody's quite sure where they got it from.
        
        Console will send at least 1 byte: 0x43.
        
        Unknown what the proper response is.
    
    Commands 0x44 to 0x53 are not known to be used for anything.
    
    KEYBOARD-STATUS:
        DOCUMENTATION INCOMPLETE
        
        This is used to read the Gamecube Keyboard's status and button presses.
        
        Console will send 3 bytes: 0x54, 0x00, 0x00.
        
        Accessory will reply with 8 bytes.
    
    Commands 0x55 to 0xFE are not known to be used for anything.