This is something of a cop-out, but almost none of the work so far on documenting these protocols is original work by me.

I have "cleaned up" some things, and restructured and renamed things, but all the actual research has been done by many people online.

If you have done some work on these protocols, and have posted it online, I have probably skimmed through your work, gleaned what I can, and integrated it into my own documentation.

What follows are a list of links I gathered when first starting on this adventure, but this is by no means exhaustive, and usually just includes the "starting points" for each console.

Nintendo:
    Nintendo Entertainment System (NES):
        https://tresi.github.io/nes/
    Super Nintendo Entertainment System (SNES):
        https://gamefaqs.gamespot.com/snes/916396-super-nintendo/faqs/5395
    Nintendo 64 (N64) and Nintendo Gamecube (GCN) and Nintendo Gameboy Advance (GBA):
        https://kthompson.gitlab.io/2016/07/26/n64-controller-protocol.html
        https://sites.google.com/site/consoleprotocols/home/nintendo-joy-bus-documentation?authuser=0
        https://os.mbed.com/users/christopherjwang/code/gamecube_controller/wiki/Homepage

Sega:
    Sega Master System (SMS) and Sega Genesis:
        https://github.com/jonthysell/SegaController/wiki/How-To-Read-Sega-Controllers
        https://github.com/SukkoPera/MegaDrivePlusPlus/wiki/Interface-Protocol-of-SEGA-MegaDrive%27s-6-Button-Controller
    Sega Saturn:
        http://forums.modretro.com/index.php?threads/saturn-controller-protocol-mk80116-mk80100.11328/
        https://nfggames.com/forum2/index.php?topic=5055.0
    Dreamcast:
        http://mc.pp.se/dc/maplewire.html
        http://mc.pp.se/dc/maplebus.html
        http://mc.pp.se/dc/controller.html
        https://www.raphnet.net/programmation/dreamcast_usb/index_en.php

Sony:
    Playstation 1 (PS1, PSX):
        https://www.raphnet.net/electronique/psx_adaptor/Playstation.txt
    Playstation 2 (PS2):
        http://store.curiousinventor.com/guides/ps2