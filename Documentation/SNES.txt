The SNES controllers are pretty simple internally, consisting of nothing more than a pair of shift registers.

The LATCH pin functions as a "reset" for the position of the shift register, and the shift register should put the first data bit on the DATA pin on the falling edge of that pulse.
The CLOCK pin advances the shift register to the next data bit, and the shift register should put the data for the next button on the rising edge of the pulse.

The LATCH pulse is ~12us, and the CLOCK pulses are ~6us long, with ~6us pauses.
These timings are pretty reliable, but you should still trigger advancing to the next bit on edges, not timings.

The LATCH pin idles at 0V, and pulses to +5V, while the CLOCK pin idles at +5V, and pulses to 0V. Data should be put on the DATA pin on the "trailing" edge of the LATCH/CLOCK pulses, as the SNES will read the data on the "leading" edge.

The DATA pin should be set to 0V for a "pressed" button, and 5V for a "released" button.

As for the actual data being transferred, the order of buttons is:
B, Y, Select, Start, Up, Down, Left, Right, A, X, L, R
The A button should be shifted out with the LATCH pulse, and the B button with the first PULSE pulse, with all the other buttons on the following PULSE pulses.