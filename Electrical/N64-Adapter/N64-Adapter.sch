EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 5C3D469D
P 4500 2450
F 0 "J2" H 4550 2967 50  0000 C CNN
F 1 "ADAPTER" H 4550 2876 50  0000 C CNN
F 2 "Connectors_IDC:IDC-Header_2x08_Pitch2.54mm_Straight" H 4500 2450 50  0001 C CNN
F 3 "~" H 4500 2450 50  0001 C CNN
	1    4500 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 5C3D46E4
P 1950 2500
F 0 "J1" H 1870 2175 50  0000 C CNN
F 1 "N64" H 1870 2266 50  0000 C CNN
F 2 "izzy:N64" H 1950 2500 50  0001 C CNN
F 3 "~" H 1950 2500 50  0001 C CNN
	1    1950 2500
	-1   0    0    1   
$EndComp
Text Label 2150 2600 0    50   ~ 0
+3V3
Text Label 2150 2500 0    50   ~ 0
DATA
Text Label 2150 2400 0    50   ~ 0
GND
Text Label 4300 2150 2    50   ~ 0
GND
Text Label 4300 2350 2    50   ~ 0
+3V3
Text Label 4300 2250 2    50   ~ 0
+3V3
Text Label 4300 2650 2    50   ~ 0
+3V3
Text Label 4300 2550 2    50   ~ 0
GND
Text Label 4300 2750 2    50   ~ 0
GND
Text Label 4300 2850 2    50   ~ 0
GND
Text Label 4800 2150 0    50   ~ 0
DATA
NoConn ~ 4300 2450
NoConn ~ 4800 2250
NoConn ~ 4800 2350
NoConn ~ 4800 2450
NoConn ~ 4800 2550
NoConn ~ 4800 2650
NoConn ~ 4800 2750
NoConn ~ 4800 2850
$EndSCHEMATC
