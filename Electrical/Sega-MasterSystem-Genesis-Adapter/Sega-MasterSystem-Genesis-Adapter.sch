EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:DB9_Male J1
U 1 1 5C3D4AC7
P 1800 2550
F 0 "J1" H 1720 1858 50  0000 C CNN
F 1 "SEGA" H 1720 1949 50  0000 C CNN
F 2 "Connectors_DSub:DSUB-9_Male_EdgeMount_Pitch2.77mm" H 1800 2550 50  0001 C CNN
F 3 " ~" H 1800 2550 50  0001 C CNN
	1    1800 2550
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 5C3D4B20
P 3650 2550
F 0 "J2" H 3700 3067 50  0000 C CNN
F 1 "ADAPTER" H 3700 2976 50  0000 C CNN
F 2 "Connectors_IDC:IDC-Header_2x08_Pitch2.54mm_Straight" H 3650 2550 50  0001 C CNN
F 3 "~" H 3650 2550 50  0001 C CNN
	1    3650 2550
	1    0    0    -1  
$EndComp
Text Label 2100 2950 0    50   ~ 0
+5V
Text Label 2100 2850 0    50   ~ 0
Data5
Text Label 2100 2750 0    50   ~ 0
Data3
Text Label 2100 2650 0    50   ~ 0
GND
Text Label 2100 2550 0    50   ~ 0
Data2
Text Label 2100 2450 0    50   ~ 0
Select
Text Label 2100 2350 0    50   ~ 0
Data1
Text Label 2100 2250 0    50   ~ 0
Data4
Text Label 2100 2150 0    50   ~ 0
Data0
Text Label 3950 2250 0    50   ~ 0
Data0
Text Label 3950 2350 0    50   ~ 0
Data1
Text Label 3950 2450 0    50   ~ 0
Data2
Text Label 3950 2550 0    50   ~ 0
Data3
Text Label 3950 2650 0    50   ~ 0
Data4
Text Label 3950 2750 0    50   ~ 0
Data5
Text Label 3950 2850 0    50   ~ 0
Select
Text Label 3450 2350 2    50   ~ 0
+5V
Text Label 3450 2550 2    50   ~ 0
+5V
Text Label 3450 2250 2    50   ~ 0
GND
Text Label 3450 2450 2    50   ~ 0
+3V3
Text Label 3450 2850 2    50   ~ 0
+3V3
Text Label 3450 2750 2    50   ~ 0
GND
Text Label 3450 2650 2    50   ~ 0
GND
Text Label 3450 2950 2    50   ~ 0
GND
$EndSCHEMATC
