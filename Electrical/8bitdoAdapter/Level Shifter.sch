EESchema Schematic File Version 4
LIBS:8bitdoAdapter-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5600 2600 5800 2600
Wire Wire Line
	5600 3100 5800 3100
Wire Wire Line
	4800 2700 4800 2600
Text HLabel 4550 3100 0    50   BiDi ~ 0
External
Text HLabel 4550 2600 0    50   Input ~ 0
External+V
Text HLabel 5800 2600 2    50   Input ~ 0
Internal+V
Text HLabel 5800 3100 2    50   BiDi ~ 0
Internal
$Comp
L Device:R R2
U 1 1 5C399445
P 4800 2850
AR Path="/5C3992A2/5C399445" Ref="R2"  Part="1" 
AR Path="/5C39AA2C/5C399445" Ref="R4"  Part="1" 
AR Path="/5C39AB37/5C399445" Ref="R6"  Part="1" 
AR Path="/5C39AB99/5C399445" Ref="R8"  Part="1" 
AR Path="/5C39AC64/5C399445" Ref="R10"  Part="1" 
AR Path="/5C39AC69/5C399445" Ref="R12"  Part="1" 
AR Path="/5C39AC76/5C399445" Ref="R14"  Part="1" 
F 0 "R14" H 4730 2804 50  0000 R CNN
F 1 "R" H 4730 2895 50  0000 R CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4730 2850 50  0001 C CNN
F 3 "~" H 4800 2850 50  0001 C CNN
	1    4800 2850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5C3E0856
P 5600 2850
AR Path="/5C3992A2/5C3E0856" Ref="R3"  Part="1" 
AR Path="/5C39AA2C/5C3E0856" Ref="R5"  Part="1" 
AR Path="/5C39AB37/5C3E0856" Ref="R7"  Part="1" 
AR Path="/5C39AB99/5C3E0856" Ref="R9"  Part="1" 
AR Path="/5C39AC64/5C3E0856" Ref="R11"  Part="1" 
AR Path="/5C39AC69/5C3E0856" Ref="R13"  Part="1" 
AR Path="/5C39AC76/5C3E0856" Ref="R15"  Part="1" 
F 0 "R15" H 5530 2804 50  0000 R CNN
F 1 "R" H 5530 2895 50  0000 R CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5530 2850 50  0001 C CNN
F 3 "~" H 5600 2850 50  0001 C CNN
	1    5600 2850
	-1   0    0    1   
$EndComp
Wire Wire Line
	5000 3100 4800 3100
Wire Wire Line
	4800 3000 4800 3100
Connection ~ 4800 3100
Wire Wire Line
	4800 3100 4550 3100
Wire Wire Line
	5400 3100 5600 3100
Wire Wire Line
	5600 3100 5600 3000
Wire Wire Line
	5600 2700 5600 2600
Wire Wire Line
	4550 2600 4800 2600
Connection ~ 5600 3100
Wire Wire Line
	5600 2600 5200 2600
Wire Wire Line
	5200 2600 5200 2800
Connection ~ 5600 2600
$Comp
L Transistor_FET:2N7002 Q1
U 1 1 5C5177FC
P 5200 3000
AR Path="/5C3992A2/5C5177FC" Ref="Q1"  Part="1" 
AR Path="/5C39AA2C/5C5177FC" Ref="Q2"  Part="1" 
AR Path="/5C39AB37/5C5177FC" Ref="Q3"  Part="1" 
AR Path="/5C39AB99/5C5177FC" Ref="Q4"  Part="1" 
AR Path="/5C39AC64/5C5177FC" Ref="Q5"  Part="1" 
AR Path="/5C39AC69/5C5177FC" Ref="Q6"  Part="1" 
AR Path="/5C39AC76/5C5177FC" Ref="Q7"  Part="1" 
F 0 "Q7" V 5450 3000 50  0000 C CNN
F 1 "2N7002" V 5541 3000 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 5400 2925 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7002.pdf" H 5200 3000 50  0001 L CNN
	1    5200 3000
	0    -1   1    0   
$EndComp
$EndSCHEMATC
