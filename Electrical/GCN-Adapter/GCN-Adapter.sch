EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x07 J1
U 1 1 5C3D4876
P 2050 2550
F 0 "J1" H 1970 2025 50  0000 C CNN
F 1 "GCN" H 1970 2116 50  0000 C CNN
F 2 "izzy:GCN" H 2050 2550 50  0001 C CNN
F 3 "~" H 2050 2550 50  0001 C CNN
	1    2050 2550
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 5C3D48C5
P 4300 2500
F 0 "J2" H 4350 3017 50  0000 C CNN
F 1 "ADAPTER" H 4350 2926 50  0000 C CNN
F 2 "Connectors_IDC:IDC-Header_2x08_Pitch2.54mm_Straight" H 4300 2500 50  0001 C CNN
F 3 "~" H 4300 2500 50  0001 C CNN
	1    4300 2500
	1    0    0    -1  
$EndComp
Text Label 2250 2750 0    50   ~ 0
DATA
Text Label 2250 2650 0    50   ~ 0
GND
Text Label 2250 2550 0    50   ~ 0
GND
Text Label 2250 2350 0    50   ~ 0
+3V3
Text Label 2250 2250 0    50   ~ 0
GND
Text Label 4100 2300 2    50   ~ 0
+3V3
Text Label 4100 2400 2    50   ~ 0
+3V3
Text Label 4100 2200 2    50   ~ 0
GND
Text Label 4100 2600 2    50   ~ 0
+3V3
Text Label 4100 2700 2    50   ~ 0
+3V3
Text Label 4100 2800 2    50   ~ 0
GND
Text Label 4100 2900 2    50   ~ 0
GND
Text Label 4600 2200 0    50   ~ 0
DATA
NoConn ~ 2250 2450
NoConn ~ 2250 2850
NoConn ~ 4600 2300
NoConn ~ 4600 2400
NoConn ~ 4600 2500
NoConn ~ 4600 2600
NoConn ~ 4600 2700
NoConn ~ 4600 2800
NoConn ~ 4600 2900
NoConn ~ 4100 2500
$EndSCHEMATC
