EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 5C3D4F12
P 4650 2500
F 0 "J2" H 4700 3017 50  0000 C CNN
F 1 "ADAPTER" H 4700 2926 50  0000 C CNN
F 2 "Connectors_IDC:IDC-Header_2x08_Pitch2.54mm_Straight" H 4650 2500 50  0001 C CNN
F 3 "~" H 4650 2500 50  0001 C CNN
	1    4650 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x05 J1
U 1 1 5C3D4F5B
P 2600 2550
F 0 "J1" H 2520 2125 50  0000 C CNN
F 1 "DREAMCAST" H 2520 2216 50  0000 C CNN
F 2 "izzy:Dreamcast" H 2600 2550 50  0001 C CNN
F 3 "~" H 2600 2550 50  0001 C CNN
	1    2600 2550
	-1   0    0    1   
$EndComp
Text Label 2800 2750 0    50   ~ 0
A
Text Label 2800 2650 0    50   ~ 0
+5V
Text Label 2800 2550 0    50   ~ 0
GND
Text Label 2800 2450 0    50   ~ 0
GND
Text Label 2800 2350 0    50   ~ 0
B
Text Label 4450 2200 2    50   ~ 0
GND
Text Label 4450 2500 2    50   ~ 0
+5V
Text Label 4450 2300 2    50   ~ 0
+3V3
Text Label 4450 2400 2    50   ~ 0
+3V3
Text Label 4450 2800 2    50   ~ 0
+3V3
Text Label 4450 2700 2    50   ~ 0
+3V3
Text Label 4450 2600 2    50   ~ 0
GND
Text Label 4450 2900 2    50   ~ 0
GND
Text Label 4950 2200 0    50   ~ 0
A
Text Label 4950 2300 0    50   ~ 0
B
NoConn ~ 4950 2400
NoConn ~ 4950 2500
NoConn ~ 4950 2600
NoConn ~ 4950 2700
NoConn ~ 4950 2800
NoConn ~ 4950 2900
$EndSCHEMATC
