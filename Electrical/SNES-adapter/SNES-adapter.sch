EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x07 J1
U 1 1 5C3D3F58
P 1300 2650
F 0 "J1" H 1220 2125 50  0000 C CNN
F 1 "SNES" H 1220 2216 50  0000 C CNN
F 2 "izzy:SNES" H 1300 2650 50  0001 C CNN
F 3 "~" H 1300 2650 50  0001 C CNN
	1    1300 2650
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 5C3D3FD7
P 4350 2650
F 0 "J2" H 4400 3167 50  0000 C CNN
F 1 "ADAPTER" H 4400 3076 50  0000 C CNN
F 2 "Connectors_IDC:IDC-Header_2x08_Pitch2.54mm_Straight" H 4350 2650 50  0001 C CNN
F 3 "~" H 4350 2650 50  0001 C CNN
	1    4350 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 2350 1500 2350
Wire Wire Line
	1600 2450 1500 2450
Wire Wire Line
	1600 2550 1500 2550
Wire Wire Line
	1600 2650 1500 2650
Wire Wire Line
	1600 2750 1500 2750
Wire Wire Line
	1600 2850 1500 2850
Wire Wire Line
	1600 2950 1500 2950
Text Label 1600 2950 0    50   ~ 0
+5V
Text Label 1600 2850 0    50   ~ 0
CLOCK
Text Label 1600 2750 0    50   ~ 0
LATCH
Text Label 1600 2650 0    50   ~ 0
DATA
Text Label 1600 2350 0    50   ~ 0
GND
NoConn ~ 1600 2450
NoConn ~ 1600 2550
Text Label 4150 2750 2    50   ~ 0
GND
Text Label 4150 2850 2    50   ~ 0
GND
Text Label 4150 2950 2    50   ~ 0
GND
Text Label 4150 3050 2    50   ~ 0
GND
Text Label 4150 2350 2    50   ~ 0
GND
Text Label 4150 2450 2    50   ~ 0
+5V
Text Label 4150 2650 2    50   ~ 0
+5V
Text Label 4650 2350 0    50   ~ 0
DATA
Text Label 4650 2450 0    50   ~ 0
LATCH
Text Label 4650 2550 0    50   ~ 0
CLOCK
NoConn ~ 4150 2550
NoConn ~ 4650 2650
NoConn ~ 4650 2750
NoConn ~ 4650 2850
NoConn ~ 4650 2950
NoConn ~ 4650 3050
$EndSCHEMATC
