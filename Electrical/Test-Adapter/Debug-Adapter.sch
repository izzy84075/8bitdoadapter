EESchema Schematic File Version 4
LIBS:Debug-Adapter-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J1
U 1 1 5C3E7092
P 3300 1950
F 0 "J1" H 3350 2467 50  0000 C CNN
F 1 "ADAPTER" H 3350 2376 50  0000 C CNN
F 2 "Connectors_IDC:IDC-Header_2x08_Pitch2.54mm_Straight" H 3300 1950 50  0001 C CNN
F 3 "~" H 3300 1950 50  0001 C CNN
	1    3300 1950
	1    0    0    -1  
$EndComp
Text Label 3100 1650 2    50   ~ 0
GND
Text Label 3100 1750 2    50   ~ 0
IN-COMM+
Text Label 3100 1850 2    50   ~ 0
+3V3
Text Label 3100 1950 2    50   ~ 0
+5V
Text Label 3100 2050 2    50   ~ 0
ID1
Text Label 3100 2150 2    50   ~ 0
ID2
Text Label 3100 2250 2    50   ~ 0
ID3
Text Label 3100 2350 2    50   ~ 0
ID4
Text Label 3600 1650 0    50   ~ 0
DATA1
Text Label 3600 1750 0    50   ~ 0
DATA2
Text Label 3600 1850 0    50   ~ 0
DATA3
Text Label 3600 1950 0    50   ~ 0
DATA4
Text Label 3600 2050 0    50   ~ 0
DATA5
Text Label 3600 2150 0    50   ~ 0
DATA6
Text Label 3600 2250 0    50   ~ 0
DATA7
NoConn ~ 3600 2350
$Comp
L Jumper:Jumper_3_Open JP2
U 1 1 5C3E7B0A
P 1450 2950
F 0 "JP2" V 1496 3037 50  0000 L CNN
F 1 "ID1" V 1405 3037 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 1450 2950 50  0001 C CNN
F 3 "~" H 1450 2950 50  0001 C CNN
	1    1450 2950
	0    -1   -1   0   
$EndComp
$Comp
L Jumper:Jumper_3_Open JP3
U 1 1 5C3E8D8A
P 2450 2950
F 0 "JP3" V 2496 3037 50  0000 L CNN
F 1 "ID2" V 2405 3037 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 2450 2950 50  0001 C CNN
F 3 "~" H 2450 2950 50  0001 C CNN
	1    2450 2950
	0    -1   -1   0   
$EndComp
$Comp
L Jumper:Jumper_3_Open JP4
U 1 1 5C3E98FA
P 1450 3950
F 0 "JP4" V 1496 4037 50  0000 L CNN
F 1 "ID3" V 1405 4037 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 1450 3950 50  0001 C CNN
F 3 "~" H 1450 3950 50  0001 C CNN
	1    1450 3950
	0    -1   -1   0   
$EndComp
$Comp
L Jumper:Jumper_3_Open JP5
U 1 1 5C3E9D9E
P 2450 3950
F 0 "JP5" V 2496 4037 50  0000 L CNN
F 1 "ID4" V 2405 4037 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 2450 3950 50  0001 C CNN
F 3 "~" H 2450 3950 50  0001 C CNN
	1    2450 3950
	0    -1   -1   0   
$EndComp
$Comp
L Jumper:Jumper_3_Open JP1
U 1 1 5C3EA009
P 1450 1300
F 0 "JP1" V 1496 1387 50  0000 L CNN
F 1 "COMM" V 1405 1387 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 1450 1300 50  0001 C CNN
F 3 "~" H 1450 1300 50  0001 C CNN
	1    1450 1300
	0    -1   -1   0   
$EndComp
Text Label 1450 1050 1    50   ~ 0
+3V3
Text Label 1450 1550 3    50   ~ 0
+5V
Text Label 1600 1300 0    50   ~ 0
IN-COMM+
Text Label 1450 2700 1    50   ~ 0
+3V3
Text Label 2450 2700 1    50   ~ 0
+3V3
Text Label 1450 3700 1    50   ~ 0
+3V3
Text Label 2450 3700 1    50   ~ 0
+3V3
Text Label 1450 3200 3    50   ~ 0
GND
Text Label 2450 3200 3    50   ~ 0
GND
Text Label 1450 4200 3    50   ~ 0
GND
Text Label 2450 4200 3    50   ~ 0
GND
Text Label 1600 2950 0    50   ~ 0
ID1
Text Label 2600 2950 0    50   ~ 0
ID2
Text Label 1600 3950 0    50   ~ 0
ID3
Text Label 2600 3950 0    50   ~ 0
ID4
$Comp
L Connector_Generic:Conn_01x07 J2
U 1 1 5C3EEC1C
P 5450 1950
F 0 "J2" H 5530 1992 50  0000 L CNN
F 1 "CTLR" H 5530 1901 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 5450 1950 50  0001 C CNN
F 3 "~" H 5450 1950 50  0001 C CNN
	1    5450 1950
	1    0    0    -1  
$EndComp
Text Label 5250 1650 2    50   ~ 0
DATA1
Text Label 5250 1750 2    50   ~ 0
DATA2
Text Label 5250 1850 2    50   ~ 0
DATA3
Text Label 5250 1950 2    50   ~ 0
DATA4
Text Label 5250 2050 2    50   ~ 0
DATA5
Text Label 5250 2150 2    50   ~ 0
DATA6
Text Label 5250 2250 2    50   ~ 0
DATA7
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 5C3F2F8B
P 5450 1300
F 0 "J3" H 5530 1342 50  0000 L CNN
F 1 "Conn_01x03" H 5530 1251 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 5450 1300 50  0001 C CNN
F 3 "~" H 5450 1300 50  0001 C CNN
	1    5450 1300
	1    0    0    -1  
$EndComp
Text Label 5250 1300 2    50   ~ 0
GND
Text Label 5250 1200 2    50   ~ 0
+3V3
Text Label 5250 1400 2    50   ~ 0
+5V
$EndSCHEMATC
