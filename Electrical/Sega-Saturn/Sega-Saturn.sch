EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 5C3D4CF6
P 4450 3400
F 0 "J2" H 4500 3917 50  0000 C CNN
F 1 "ADAPTER" H 4500 3826 50  0000 C CNN
F 2 "Connectors_IDC:IDC-Header_2x08_Pitch2.54mm_Straight" H 4450 3400 50  0001 C CNN
F 3 "~" H 4450 3400 50  0001 C CNN
	1    4450 3400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x09 J1
U 1 1 5C3D4D41
P 2250 3500
F 0 "J1" H 2170 2875 50  0000 C CNN
F 1 "SATURN" H 2170 2966 50  0000 C CNN
F 2 "izzy:Saturn" H 2250 3500 50  0001 C CNN
F 3 "~" H 2250 3500 50  0001 C CNN
	1    2250 3500
	-1   0    0    1   
$EndComp
Text Label 2450 3900 0    50   ~ 0
+5V
Text Label 2450 3800 0    50   ~ 0
Data1
Text Label 2450 3700 0    50   ~ 0
Data0
Text Label 2450 3600 0    50   ~ 0
Select1
Text Label 2450 3500 0    50   ~ 0
Select0
Text Label 2450 3400 0    50   ~ 0
Acknowledge
Text Label 2450 3300 0    50   ~ 0
Data3
Text Label 2450 3200 0    50   ~ 0
Data2
Text Label 2450 3100 0    50   ~ 0
GND
Text Label 4250 3100 2    50   ~ 0
GND
Text Label 4250 3200 2    50   ~ 0
+5V
Text Label 4250 3400 2    50   ~ 0
+5V
Text Label 4250 3300 2    50   ~ 0
+3V3
Text Label 4250 3700 2    50   ~ 0
+3V3
Text Label 4250 3500 2    50   ~ 0
+3V3
Text Label 4250 3600 2    50   ~ 0
GND
Text Label 4250 3800 2    50   ~ 0
GND
Text Label 4750 3100 0    50   ~ 0
Data0
Text Label 4750 3200 0    50   ~ 0
Data1
Text Label 4750 3300 0    50   ~ 0
Data2
Text Label 4750 3400 0    50   ~ 0
Data3
Text Label 4750 3500 0    50   ~ 0
Acknowledge
Text Label 4750 3600 0    50   ~ 0
Select0
Text Label 4750 3700 0    50   ~ 0
Select1
NoConn ~ 4750 3800
$EndSCHEMATC
