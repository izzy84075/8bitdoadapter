EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 5C3D5117
P 5050 2500
F 0 "J2" H 5100 3017 50  0000 C CNN
F 1 "ADAPTER" H 5100 2926 50  0000 C CNN
F 2 "Connectors_IDC:IDC-Header_2x08_Pitch2.54mm_Straight" H 5050 2500 50  0001 C CNN
F 3 "~" H 5050 2500 50  0001 C CNN
	1    5050 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x09 J1
U 1 1 5C3D5160
P 2850 2500
F 0 "J1" H 2770 1875 50  0000 C CNN
F 1 "PSX" H 2770 1966 50  0000 C CNN
F 2 "izzy:Playstation" H 2850 2500 50  0001 C CNN
F 3 "~" H 2850 2500 50  0001 C CNN
	1    2850 2500
	-1   0    0    1   
$EndComp
Text Label 3050 2900 0    50   ~ 0
MISO
Text Label 3050 2800 0    50   ~ 0
MOSI
Text Label 3050 2600 0    50   ~ 0
GND
Text Label 3050 2500 0    50   ~ 0
+3V3
Text Label 3050 2400 0    50   ~ 0
CS
Text Label 3050 2300 0    50   ~ 0
SCK
Text Label 3050 2100 0    50   ~ 0
Acknowledge
Text Label 4850 2300 2    50   ~ 0
+3V3
Text Label 4850 2400 2    50   ~ 0
+3V3
Text Label 4850 2200 2    50   ~ 0
GND
Text Label 4850 2900 2    50   ~ 0
+3V3
Text Label 4850 2800 2    50   ~ 0
GND
Text Label 4850 2700 2    50   ~ 0
GND
Text Label 4850 2600 2    50   ~ 0
GND
Text Label 5350 2500 0    50   ~ 0
CS
Text Label 5350 2400 0    50   ~ 0
SCK
Text Label 5350 2200 0    50   ~ 0
MOSI
Text Label 5350 2300 0    50   ~ 0
MISO
Text Label 5350 2600 0    50   ~ 0
Acknowledge
$EndSCHEMATC
