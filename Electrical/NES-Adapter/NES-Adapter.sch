EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x07 J1
U 1 1 5C3D447B
P 1650 2750
F 0 "J1" H 1570 2225 50  0000 C CNN
F 1 "NES" H 1570 2316 50  0000 C CNN
F 2 "izzy:NES" H 1650 2750 50  0001 C CNN
F 3 "~" H 1650 2750 50  0001 C CNN
	1    1650 2750
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 5C3D44D2
P 3000 2700
F 0 "J2" H 3050 3217 50  0000 C CNN
F 1 "ADAPTER" H 3050 3126 50  0000 C CNN
F 2 "Connectors_IDC:IDC-Header_2x08_Pitch2.54mm_Straight" H 3000 2700 50  0001 C CNN
F 3 "~" H 3000 2700 50  0001 C CNN
	1    3000 2700
	1    0    0    -1  
$EndComp
Text Label 1850 3050 0    50   ~ 0
GND
Text Label 1850 2950 0    50   ~ 0
CLOCK
Text Label 1850 2850 0    50   ~ 0
LATCH
Text Label 1850 2750 0    50   ~ 0
DATA
Text Label 1850 2450 0    50   ~ 0
+5V
Text Label 2800 2400 2    50   ~ 0
GND
Text Label 2800 2500 2    50   ~ 0
+5V
Text Label 2800 2700 2    50   ~ 0
+5V
Text Label 2800 2600 2    50   ~ 0
+3V3
Text Label 2800 2800 2    50   ~ 0
+3V3
Text Label 2800 2900 2    50   ~ 0
GND
Text Label 2800 3000 2    50   ~ 0
GND
Text Label 2800 3100 2    50   ~ 0
GND
Text Label 3300 2400 0    50   ~ 0
DATA
Text Label 3300 2500 0    50   ~ 0
LATCH
Text Label 3300 2600 0    50   ~ 0
CLOCK
NoConn ~ 3300 2700
NoConn ~ 3300 2800
NoConn ~ 3300 2900
NoConn ~ 3300 3000
NoConn ~ 3300 3100
NoConn ~ 1850 2650
NoConn ~ 1850 2550
$EndSCHEMATC
